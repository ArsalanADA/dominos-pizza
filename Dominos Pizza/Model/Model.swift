//
//  Model.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 24.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation

struct MODEL {
    var food: Food
    var ingredients: [Ingredient]
    var isSpecial: Bool
}

var model: [MODEL] = []
