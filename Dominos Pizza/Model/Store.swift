//
//  Store.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 29.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation

struct Store {
    var locationCode: String
    var name: String
    var address: String
    var IPAddress: String
    var longitude: Double
    var lattitude: Double
    var availableFrom: String
    var availableUntil: String
}
