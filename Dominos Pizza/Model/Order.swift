//
//  Order.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 23.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation

struct Order: Equatable {
    var food: Food
    var quantity: Int
    var description: String { return "Food: \(food), Quantity: \(quantity)" }
    
    static func ==(lhs: Order, rhs: Order) -> Bool {
        if !(lhs.food is AbstractPizza) {
            if lhs.food.productCode == rhs.food.productCode {
                return true
            }
        } else { // if orders are pizza
            if lhs.food.productCode == rhs.food.productCode {
                if let pizza1 = lhs.food as? AbstractPizza, let pizza2 = rhs.food as? AbstractPizza {
                    if pizza1.selectedToppings() == pizza2.selectedToppings() {
                        if pizza1.selectedSize == pizza2.selectedSize {
                            if pizza1.pizzas[pizza1.selectedSize.rawValue].selectedExtraIngredients() == pizza2.pizzas[pizza2.selectedSize.rawValue].selectedExtraIngredients() {
                                if pizza1.pizzas[pizza1.selectedSize.rawValue].selectedRim() == pizza2.pizzas[pizza2.selectedSize.rawValue].selectedRim() {
                                    if pizza1.pizzas[pizza1.selectedSize.rawValue].selectedDough() == pizza2.pizzas[pizza2.selectedSize.rawValue].selectedDough() {
                                        return true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false
    }
}

enum DeliveryMethod: Int {
    case carryOut = 2, delivery = 1
}

enum PaymentMethod: Int {
    case cash = 3, card = 4
}
