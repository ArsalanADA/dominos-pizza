//
//  Promotion.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 09.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation
import UIKit

struct Promotion: Codable {
    var CouponCode: String?
    var SortListCode: String?
    var Description: String?
    var DiscountValue: String?
    var DiscountlessTotal: String?
    var GroupSequence: String?
}

struct PromotionProduct: Codable {
    let CouponCode: String?
    let GroupSequence: String?
    let ProductSequence: String?
    let ProductFlavorCode: String?
    let ProductSizeCode: String?
    let ProductOsgCode: String?
    let Description: String?
    let SurchargelessPrice: String?
    let Surcharge: String?
    let CategoryCode: String?
}
