//
//  Address.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 19.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation

struct MyAddress {
    var isFavorite: Bool
    var apartment: String?
    var cityCode: String?
    var countryCode: String?
    var customerID: String?
    var customerAddressID: String?
    var dictrictCode: String?
    var doorNumber: String?
    var instructions: String?
    var phone: String?
    var streetName: String?
    var regionName: String?
    var locationCode: String?
}

struct Address {
    var cityCode: String?
    var countyCode: String?
    var districtCode: String?
    var regionName: String?
    var isActive: Bool?
    var streets: [Street]?
}

struct Street {
    var streetCode: String?
    var cityCode: String?
    var countryCode: String?
    var name: String?
}
