//
//  HistoryOrder.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.05.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation

struct HistoryOrder {
    let id: String
    let text: String
    let status: String
    let store: String
    let price: Double
    let date: String
}

/*
 "OrderID": "23787",
 "OrderText": "1 adet Medium Americano,Classic Dough,Classic (Ketchup istiyorumGreen Pepper istiyorumMozzarella Cheese istiyorumMushrooms istiyorumPizza Sauce istiyorumGrilled Chicken istiyorum)",
 "Status": "New Order",
 "Store": "Nərimanov",
 "PaymentPrice": "15.2",
 "CreatedOn": "2018-05-14 13:05:52"
 */
