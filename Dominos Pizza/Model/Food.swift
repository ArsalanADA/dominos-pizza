//
//  Food.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 20.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation
import UIKit

class Food: Hashable {
    
    var hashValue: Int
    var name: String
    var image: UIImage?
    var productCode: String
    var isFavorite: Bool
    var surchargelessPrice: Double
    var surcharge: Double
    var productSizeCode: String?
    
    init(name: String, image: UIImage?, productCode: String, isFavorite: Bool? = false, surcharge: Double, surchargelessPrice: Double, productSizeCode: String?) {
        self.name = name
        self.image = image
        self.productCode = productCode
        self.isFavorite = isFavorite!
        self.hashValue = Int(arc4random())
        self.surchargelessPrice = surchargelessPrice
        self.surcharge = surcharge
        self.productSizeCode = productSizeCode
    }
    
    static func ==(lhs: Food, rhs: Food) -> Bool {
        if lhs.productCode == rhs.productCode {
            return true
        }
        return false
    }
}
