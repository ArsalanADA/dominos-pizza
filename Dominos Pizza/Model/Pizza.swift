//
//  Pizza.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 05.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit
import SwiftyJSON

class AbstractPizza: Food {
    
    var pizzas: [KindOfPizza]
    var ingredients: [Ingredient]
    var products: [Product]
    var selectedSize: Size

    init(name: String, code: String, pizzas: [KindOfPizza], image: UIImage?, ingredients: [Ingredient], products: [Product], selectedSize: Size, productSizeCode: String?) {
        self.pizzas = pizzas
        self.ingredients = ingredients
        self.products = products
        self.selectedSize = selectedSize

        super.init(name: name, image: image, productCode: code, surcharge: -1, surchargelessPrice: -1, productSizeCode: productSizeCode)
    }
    
    func selectedToppings() -> [Ingredient]{
        var arra: [Ingredient] = []
        for ingredient in ingredients {
            if ingredient.isSelected {
                arra.append(ingredient)
            }
        }
        return arra
    }

    func setToClassicRim() {
        // disselect all
        for p in 0 ..< pizzas.count {
            for r in 0 ..< pizzas[p].rim.count {
                pizzas[p].rim[r].isSelected = false
            }
        }

        // select only classic rim
        for p in 0 ..< pizzas.count {
            for r in 0 ..< pizzas[p].rim.count {
                if pizzas[p].rim[r].sideCode == "NONE" {
                    pizzas[p].rim[r].isSelected = true
                }
            }
        }
    }
    
    func remove(_ topping: Ingredient) {
        for i in 0 ..< ingredients.count {
            if topping == ingredients[i] {
                ingredients[i].isSelected = false
            }
        }
    }
}

struct KindOfPizza {
    var dough: [Dough]
    var rim: [Rim]
    var extraIngredients: [Ingredient]
    
    func selectedExtraIngredients() -> [Ingredient] {
        var array: [Ingredient] = []
        for ingredient in extraIngredients {
            if ingredient.isSelected {
                array.append(ingredient)
            }
        }
        return array
    }

    func selectedDough() -> Dough? {
        for dough in dough {
            if dough.isSelected {
                return dough
            }
        }
        return nil
    }

    func selectedRim() -> Rim? {
        for rim in rim {
            if rim.isSelected {
                return rim
            }
        }
        return nil
    }
    
    mutating func removeExtraIngredient(_ ingredient: Ingredient) {
        for i in 0 ..< extraIngredients.count {
            if ingredient == extraIngredients[i] {
                extraIngredients[i].isSelected = false
            }
        }
    }
    
}

enum Size: Int {
    case small = 0, medium, big
}

enum PromotionSize: String {
    case small = "8", medium = "10", big = "12", litr = "2LTB"
}

struct PizzaSize {
    let size: Size
    let image: UIImage
    var price: String
    var isSelected: Bool
}

struct Dough: Equatable {
    let name: String
    let code: String
    let image: UIImage
    let price: Double
    var isSelected: Bool
}

struct Rim: Equatable {
    let name: String
    let sideCode: String
    let price: Double
    let image: UIImage
    var isSelected: Bool
}

struct Ingredient: Equatable {
    let name: String
    let price: Double
    var isSelected: Bool
    let productOptionCode: String
}

struct Product {
    let productCode: String
    let description: String
    let categoryCode: String
    let pulseCatCode: String
    let flavorCode: String
    let flavorName: String
    let osgCode: String
    let osgName: String
    let doughCode: String
    let doughName: String
    let sideCode: String
    let sideName: String
    let sizeCode: String
    let sizeName: String
    let price: Double
    let size: Size
    let surchargelessPrice: Double
    let surcharge: Double
}














