//
//  User.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 19.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation

enum Sex: Int {
    case male = 0, female = 1
}

class User: NSObject, NSCoding  {
    
    var token: String
    var id: String?
    
    var firstname: String = ""
    var lastname: String = ""
    
    var email: String = ""
    
    var sex: Sex = .male
    var birthdate: Int?
    var phone: String = ""
    
    var channelCode: String = "IPHONE"
    var ip: String = "255.255.255.255"

    func selectedAddress() -> MyAddress? {
        for address in myAddresses {
            if address.isFavorite {
                return address
            }
        }
        return nil
    }

    init(firstname: String, lastname: String, email: String, sex: Sex, phone: String, token: String, id: String?, birthdate: Int?) {
        self.token = token
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.birthdate = birthdate
        self.phone = phone
        self.channelCode = "IPHONE"
        self.token = token
        self.id = id
    }

    required init(coder decoder: NSCoder) {
        self.firstname = decoder.decodeObject(forKey: "firstname") as? String ?? ""
        self.lastname = decoder.decodeObject(forKey: "lastname") as? String ?? ""
        self.email = decoder.decodeObject(forKey: "email") as? String ?? ""
        self.birthdate = decoder.decodeObject(forKey: "birthdate") as? Int ?? 0
        self.phone = decoder.decodeObject(forKey: "phone") as? String ?? ""
        self.sex = decoder.decodeObject(forKey: "sex") as? Sex ?? .male
        self.token = decoder.decodeObject(forKey: "token") as? String ?? ""
        self.id = decoder.decodeObject(forKey: "id") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(firstname, forKey: "firstname")
        coder.encode(lastname, forKey: "lastname")
        coder.encode(email, forKey: "email")
        coder.encode(birthdate, forKey: "birthdate")
        coder.encode(phone, forKey: "phone")
        coder.encode(sex.rawValue, forKey: "sex")
        coder.encode(token, forKey: "token")
        coder.encode(id, forKey: "id")
    }
    
}






