//
//  LoginController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 19.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import Localize_Swift
import SwiftyJSON

class LoginController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var activityindicator: UIActivityIndicatorView!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
    }
    
    func localize() {
        passwordLabel.text = "password".localized()
        emailLabel.text = "email".localized()
        loginButton.setTitle("login".localized(), for: .normal)
    }
    
    @IBAction func login() {
        
        activityindicator.startAnimating()
        
        let loginUrl = baseURL + "login"
        
        let email = emailTextField.text
        let password = passwordTextField.text
        
        let parameters: Parameters = ["email": email!, "password": password!]
        
        Alamofire.request(loginUrl, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: [:]).responseJSON { (response) in

            self.activityindicator.stopAnimating()

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)

                    // FIXME: nul
                    print(json["data"]["userData"])
                    
                    let success = json["data"]["success"].int
                    let message = json["data"]["message"].string
                    
                    print(message ?? "no message")

                    // FIXME: null
                    if success == 0 {
                        let alert = UIAlertController(title: "Problem", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "tab")
                        self.present(vc!, animated: true, completion: nil)
                        
                        let name = json["data"]["userData"]["firstname"].stringValue
                        let lastname = json["data"]["userData"]["lastname"].stringValue
                        let email = json["data"]["userData"]["email"].stringValue
                        let token = json["data"]["userData"]["token"].stringValue
                        let phone = json["data"]["userData"]["phone"].stringValue
                        let sex: Sex = .male
                        let id = json["data"]["userData"]["customerId"].stringValue
                        let timestamp = json["data"]["userData"]["birthdate"].intValue
                        
                        let user = User(firstname: name, lastname: lastname, email: email, sex: sex, phone: phone, token: token, id: id, birthdate: timestamp)
                        
                        // remove previous (optional)
                        UserDefaults.standard.removeObject(forKey: userKeyString)

                        // Save in user defaults
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
                        UserDefaults.standard.set(encodedData, forKey: userKeyString)
                        print("User saved in User Defaults")
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
