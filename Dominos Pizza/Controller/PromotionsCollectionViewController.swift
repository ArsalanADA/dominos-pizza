//
//  PromotionsCollectionViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 08.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Localize_Swift

private let reuseIdentifier = "promotionCell"

var promotions: [Promotion] = []

class PromotionsCollectionViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: .promotionsDownloaded, object: nil)
        
        setupLayoutForCollectionView()
        self.navigationItem.title = "promotions".localized()

        downloadPromotions()
        
        // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage( #imageLiteral(resourceName: "cartIcon"), for: .normal)
        rightButton.tintColor = .white
        rightButton.setTitleColor(.white, for: .normal)
        rightButton.addTarget(self, action: #selector(showBasket), for: .touchUpInside)
        
        // Bar button item
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtomItem
    }
    
    @objc func showBasket() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "basketSuper") {
            super.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @objc func refresh() {
        collectionView.reloadData()
    }

    func setupLayoutForCollectionView() {
        let numberOfCellInARow: CGFloat = 2
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 20
        let widthOfCell = (UIScreen.main.bounds.width - layout.sectionInset.left - layout.sectionInset.right - ((numberOfCellInARow - 1) * layout.minimumInteritemSpacing )) / numberOfCellInARow
        let cellSize = CGSize(width: widthOfCell, height: 240)
        layout.itemSize = cellSize
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SelectPromotionViewController {
            let promotionsObject = delivaryMethod == .carryOut ? promotions.filter({$0.SortListCode == "GelAl"}) : promotions.filter({$0.SortListCode == "SCPN"})
            destination.promotion = promotionsObject[selectedPromotionIndex]
        }
    }

    var selectedPromotionIndex = -1
}

extension PromotionsCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return delivaryMethod == .carryOut ? promotions.filter({$0.SortListCode == "GelAl"}).count : promotions.filter({$0.SortListCode == "SCPN"}).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PromotionCell
        let promotionsObject = delivaryMethod == .carryOut ? promotions.filter({$0.SortListCode == "GelAl"}) : promotions.filter({$0.SortListCode == "SCPN"})
        
        cell.promotionTitle.text = promotionsObject[indexPath.row].Description
        cell.promotion = promotionsObject[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPromotionIndex = indexPath.row
        performSegue(withIdentifier: "showPromotionSelection", sender: self)
    }
    
}




