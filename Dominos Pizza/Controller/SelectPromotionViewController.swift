//
//  SelectPromotionViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 24.04.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Localize_Swift
import Alamofire
import SwiftyJSON
import Foundation

var promotionOrder = Array<Order?>(repeating: nil, count: 0)

var indexOfSelectedProduct = -1

class SelectPromotionViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var elaveEtBUtton: UIButton!
    
    var promotion: Promotion? {
        didSet {
            downloadProducts(with: promotion?.CouponCode ?? "")
        }
    }
    
    var promotionLayout: PromotionProductFlowLayout!
    
    var products: [PromotionProduct]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        promotionOrder = Array<Order?>(repeating: nil, count: Int(promotion?.GroupSequence ?? "0") ?? 0)
        
        promotionLayout = PromotionProductFlowLayout(numberOfColumns: 2)
        self.collectionView.setCollectionViewLayout(promotionLayout, animated: false)
        self.collectionView.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(newProductAdded), name: .newProductAdded, object: nil)
        
        // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage( #imageLiteral(resourceName: "cartIcon"), for: .normal)
        rightButton.tintColor = .white
        rightButton.setTitleColor(.white, for: .normal)
        rightButton.addTarget(self, action: #selector(showBasket), for: .touchUpInside)
        
        // Bar button item
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtomItem
        
    }
    
    @objc func showBasket() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "basketSuper") {
            super.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @objc func newProductAdded() {
        self.collectionView.reloadData()
    }
    
    func downloadProducts(with couponCode: String) {
        let url = baseURL + "campaign/\(couponCode)?language=\(Localize.currentLanguage())"
        Alamofire.request(url).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                do {
                    let decoder = JSONDecoder()
                    guard let data = response.data else {return}
                    let response = try decoder.decode(PromotionProductResponse.self, from: data)
                    self.products = response.data?.products!
                    NotificationCenter.default.post(name: .promotionProductsDownloaded, object: nil)
                } catch {}
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func addToOrderPressed() {
        for order in promotionOrder {
            if order == nil {
                return
            }
        }
        
        let point: CGPoint = elaveEtBUtton.center
        
        promotionOrder.forEach { (order) in
            let view: UIImageView = UIImageView(image: order?.food.image)
            animate(view: view, fromPoint: point, toPoint: CGPoint.init(x: UIScreen.main.bounds.size.width - 20, y: -10))
            orders.append(order!)
        }
    }
    
    func animate(view: UIView, fromPoint start: CGPoint, toPoint end: CGPoint) {
        
        let view: UIImageView = UIImageView(image: foodObject?.image ?? UIImage())
        view.frame = CGRect(x: 55, y: 300, width: 20, height: 20)
        view.layer.cornerRadius = view.frame.size.height / 2.0
        view.clipsToBounds = true
        
        self.view.addSubview(view)
        
        let path = UIBezierPath()
        
        path.move(to: start)
        
        let controlPoint1 = CGPoint(x: start.x + 20, y: start.y - 100)
        let controlPoint2 = CGPoint(x: start.x + 50, y: start.y - 200)
        
        path.addCurve(to: end, controlPoint1: controlPoint1, controlPoint2: controlPoint2)
        
        // create the animation
        let animation = CAKeyframeAnimation(keyPath: "position")
        animation.path = path.cgPath
        animation.rotationMode = kCAAnimationRotateAuto

        // to complete one animation
        animation.duration = 1
        
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards

        UIView.animate(withDuration: 2, delay: 0, options: [], animations: {
            view.layer.position = end
        }) { (completed) in
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ProductsCollectionController {
            var modelArray: [MODEL] = []
            guard products != nil else {return}
            let productsObject: [PromotionProduct] = products!.filter({$0.GroupSequence == "\(indexOfSelectedProduct + 1)"})
            
            for product in productsObject {
                var name = "No name"
                if product.Description != nil {
                    name = product.Description!
                } else if product.ProductFlavorCode != nil {
                    name =  product.ProductFlavorCode!
                }
                
                var productCode = product.ProductOsgCode ?? "no code"
                if product.ProductOsgCode != nil {
                    productCode = product.ProductOsgCode!
                }
                
                let modelOBJ = MODEL(food: Sous(name: name, image: nil, productCode: productCode, isFavorite: false, surcharge: Double(product.Surcharge ?? "0") ?? -1, surchargelessPrice: Double(product.SurchargelessPrice ?? "0.0") ?? -1, productSizeCode: product.ProductSizeCode ?? "-1"), ingredients: [], isSpecial: false)
                
                modelArray.append(modelOBJ)
            }
            
            model = modelArray
            destination.isPizzaSelected = productsObject[0].CategoryCode == "Pizza"
            destination.isSelectingProduct = true
        }
    }
    
    
    
}

extension SelectPromotionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard promotion != nil else {return 0}
        return Int(promotion?.GroupSequence ?? "0") ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return promotionLayout.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectPromotionCell", for: indexPath) as! SelectPromotionCell
        
        cell.imageView.image = promotionOrder[indexPath.row]?.food.image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexOfSelectedProduct = indexPath.row
        performSegue(withIdentifier: "products", sender: self)
    }
    
    
    
}


