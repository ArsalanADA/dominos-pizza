//
//  PizzaTrackerController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 22.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PizzaTrackerController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet var progressImageView: UIImageView!
    @IBOutlet var searchLabel: UILabel!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var backView: UIViewX!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var titleView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.center = progressImageView.center

        self.navigationItem.titleView = titleView
        searchLabel.text = "search".localized()
        submitButton.setTitle("submit".localized(), for: .normal)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let lastTrackingCode = UserDefaults.standard.value(forKey: trackingCodeKey) as? String {
            searchTextField.text = lastTrackingCode
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchTextField.resignFirstResponder()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func setImage(with state: Int) {
        switch state {
        case 1:
            self.progressImageView.image = #imageLiteral(resourceName: "state1")
        case 2:
            self.progressImageView.image = #imageLiteral(resourceName: "state2")
        case 3:
            self.progressImageView.image = #imageLiteral(resourceName: "state3")
        case 4:
            self.progressImageView.image = #imageLiteral(resourceName: "state4")
        case 5:
            self.progressImageView.image = #imageLiteral(resourceName: "state5")
        case 6:
            self.progressImageView.image = #imageLiteral(resourceName: "state6")
        default:
            break
        }
    }

    @IBAction func submit() {
        self.activityIndicator.startAnimating()
        
        guard let user = currentUser() else {return}

        let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]
        
        let url = baseURL + "order-tracking/\(searchTextField.text ?? "no code was written")"
        print(url)

        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response) in
            //gg33ea

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print(json["data"])
                    let statusCode = json["data"]["tracking"]["StatusCode"].intValue
                    self.setImage(with: statusCode)
                }
            case .failure(let error):
                print(error)
            }

            self.activityIndicator.stopAnimating()
        }
    }
}








