//
//  LoadingController.swift
//  
//
//  Created by Arsalan Iravani on 09.01.2018.
//

import UIKit
import Foundation
import SystemConfiguration

class LoadingController: UIViewController {
    
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var car: UIImageView!
    @IBOutlet var noInternetConnectionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadAllProducts()
        downloadMyAddresses()
        downloadHistoryOrders()
        noInternetConnectionLabel.adjustsFontForContentSizeCategory = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isInternetAvailable() {
            progressView.trackTintColor = .white
            progressView.progressTintColor = UIColor(red:0.1882, green:0.6902, blue:0.8980, alpha:1.0000)
            
            self.progressView.layoutIfNeeded()
            
            UIView.animate(withDuration: 3, animations: {
                self.car.frame.origin.x += self.progressView.bounds.width
                self.progressView.setProgress(1, animated: true)
            }) { (done) in
                self.performSegue(withIdentifier: "segueToMain", sender: nil)
                
                UIView.animate(withDuration: 1, delay: 2, options: [], animations: {
                    self.car.frame.origin.x -= self.progressView.bounds.width
                    self.progressView.setProgress(0, animated: false)
                }, completion: nil)
                
            }
        } else {
            car.isHidden = true
            progressView.isHidden = true
            noInternetConnectionLabel.isHidden = false
            print("no connection")
        }
    }
    
    func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
       
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }

}
