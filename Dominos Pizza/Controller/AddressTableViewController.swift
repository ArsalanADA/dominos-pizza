//
//  AddressTableViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

var myAddresses: [MyAddress] = []

class AddressTableViewController: UITableViewController {
    
    var indexSelected: Int = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        
        isAddingNew = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(addressesDownloaded), name: .myAddressDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addressDeleted), name: .addressDeleted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(starPresesd), name: .starPressed, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        isAddingNew = false
    }

    @objc func starPresesd() {
        self.tableView.reloadData()
    }

    @objc func addressDeleted() {
        var viewControllers = self.navigationController?.viewControllers
        viewControllers?.removeLast(2)
        self.navigationController?.setViewControllers(viewControllers!, animated: true)

        downloadMyAddresses()
        tableView.reloadData()
    }

    @objc func addressesDownloaded() {
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        downloadMyAddresses()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if myAddresses.isEmpty {
            return 1
        }
        return myAddresses.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if myAddresses.isEmpty {
            return self.view.frame.height + 10
        }
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if myAddresses.isEmpty {
            return
        }
        // FIXME: shows address
        NotificationCenter.default.post(name: .addressSelected, object: nil, userInfo: ["index" : indexPath.row])
    }

//    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
//        return false
//    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if myAddresses.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! AddressCell
        cell.indexOfAddress = indexPath.row
        cell.addressTitleLabel.text = myAddresses[indexPath.row].streetName

        if myAddresses.count > 1 {
            cell.starButton.isHidden = false
        }

        return cell
    }
    
    
    
    
    
}






