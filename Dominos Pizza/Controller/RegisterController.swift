//
//  RegisterController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 12.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import Localize_Swift
import SwiftyJSON

class RegisterController: UIViewController {
    
    let picker = UIDatePicker()
    let pickerPhoneCode = UIPickerView()
    
    
    @IBOutlet weak var fistnameLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var birthdateLabel: UILabel!
    
    @IBOutlet weak var signupButton: UIButtonX!
    
    
    @IBOutlet var firstnameTextField: UITextField!
    @IBOutlet var lastnameTextField: UITextField!
    @IBOutlet weak var phoneCodeTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var confirmTextField: UITextField!
    @IBOutlet weak var birthdateTextField: UITextField!
    
    @IBOutlet var activityindicator: UIActivityIndicatorView!
    
    
    var timestamp: TimeInterval?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Register".localized()
        
        createDatePicker()
        createPhoneCodePicker()
        localize()
    }
    
    func localize() {
        fistnameLabel.text = "firstname".localized()
        lastnameLabel.text = "lastname".localized()
        passwordLabel.text = "password".localized()
        confirmLabel.text = "confirm".localized()
        phoneLabel.text = "phone".localized()
        emailLabel.text = "email".localized()
        birthdateLabel.text = "birthdate".localized()
        signupButton.setTitle("signup".localized(), for: .normal)
    }
    
    func createPhoneCodePicker() {
        phoneCodeTextField.text = numberCodes[0]
        phoneCodeTextField.inputView = pickerPhoneCode
        pickerPhoneCode.delegate = self
        pickerPhoneCode.dataSource = self
    }
    
    @objc func donePressed() {
        // format date
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        let date = picker.date
        timestamp = date.timeIntervalSince1970
        
        let dateString = formatter.string(from: date)
        
        birthdateTextField.text = "\(dateString)"
        self.view.endEditing(true)
    }
    
    @IBAction func signUp() {
        
        activityindicator.startAnimating()
        let signUpURL = baseURL + "user/signup"
        
        let firstName = firstnameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let lastname = lastnameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let phone = (phoneCodeTextField.text ?? "") + (phoneTextField.text ?? "")
        let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let confirm = confirmTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let sex: Sex = .male
        
        let parameters : Parameters = ["firstname": firstName ?? "no name", "lastname": lastname ?? "no lastname", "email": email ?? "no email", "sex": sex.rawValue, "channel_code": "IPHONE", "phone": phone , "password": password ?? "", "confirm": confirm ?? "", "birthdate": timestamp ?? "0", "ip": ""]

        let url =  URL(string: signUpURL)!
        
        _ = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: [:]).responseJSON { (response) in
            
            self.activityindicator.stopAnimating()
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    print(json["data"])
                    
                    let success = json["data"]["success"].int
                    let message = json["data"]["message"].string
                    
                    print(message ?? "no message")
                    
                    if success == 0 {
                        let alert = UIAlertController(title: "Problem", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        
                        guard let firstname = json["data"]["userData"]["firstname"].string,
                            let lastname = json["data"]["userData"]["lastname"].string,
                            let email = json["data"]["userData"]["email"].string,
                            let token = json["data"]["userData"]["token"].string
                            else {return}
                        
                        var timestamp = 0
                        if let t = json["data"]["userData"]["token"].int {
                            timestamp = t
                        }
                        
                        let user = User(firstname: firstname, lastname: lastname, email: email, sex: sex, phone: phone, token: token, id: nil, birthdate: timestamp)
                        
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
                        UserDefaults.standard.set(encodedData, forKey: userKeyString)
                        NotificationCenter.default.post(name: .userChanged, object: nil)
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "tab")
                        self.present(vc!, animated: true, completion: nil)
                        
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func createDatePicker() {
        // toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for toolbar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        birthdateTextField.inputAccessoryView = toolbar
        birthdateTextField.inputView = picker
        
        // format picker for date
        picker.datePickerMode = .date
    }
    
}


extension RegisterController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numberCodes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        phoneCodeTextField.text = numberCodes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return numberCodes[row]
    }
    
}
