//
//  CheckoutViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 21.04.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(orderSubmitted(notification:)), name: .orderSubmitted, object: nil)
    }

    @objc func orderSubmitted(notification: Notification) {
        guard let message: String = notification.userInfo?["message"] as? String else {return}
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            
            // clear card
            orders.removeAll()
            
            // dismiss
            var viewControllers = self.navigationController?.viewControllers
            viewControllers?.removeLast(2)
            self.navigationController?.setViewControllers(viewControllers!, animated: true)
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

}
