//
//  ContainerVC.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 09.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {
    
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    @IBOutlet var customTitleView: UIView!
    
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        
        self.navigationItem.titleView = customTitleView
        
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: .toggleMenu, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showLogin), name: .showLogin, object: nil)
    }
    
    @objc func showLogin() {
        performSegue(withIdentifier: "login", sender: nil)
    }
    
    @IBAction  func toggleSideMenu() {
        if sideMenuOpen {
            sideMenuOpen = false
            self.sideMenuConstraint.constant = -240
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        } else {
            sideMenuOpen = true
            self.sideMenuConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
