//
//  AddressInfoController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

var addresses: [Address] = []

var selectedTextFieldTag = -1

var selectedIndexOfRegion: Int?
var selectedIndexOfStreet: Int?

class AddressInfoController: UIViewController, CLLocationManagerDelegate {

    var locationManager = CLLocationManager()
    let picker = UIPickerView()
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var regionTextField: UITextField!
    @IBOutlet var streetTextField: UITextField!
    
    @IBOutlet var apartmentTextField: UITextField!
    @IBOutlet var doorNumberTextField: UITextField!
    @IBOutlet weak var additionalInstructions: UITextView!
    
    @IBOutlet var button: UIButton!

    var indexOfAddress: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        createDatePicker()
        
        localize()
        adjust()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addressDownloaded), name: .addressDownloaded, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        downloadAddresses()
        
        if isAddingNew {
            button.setTitle("Add", for: .normal)
            streetTextField.isEnabled = false
        } else {
            button.setTitle("Save", for: .normal)
        }
    }
    
    @IBAction func buttonPressed() {
        
       guard let user = currentUser() else {return}
        
        if isAddingNew {
            let url = baseURL + "user/address"
            
            guard let indexOfRegion = selectedIndexOfRegion else {return}
            guard let indexOfStreet = selectedIndexOfStreet else {return}
            
            let parameters: Parameters = ["CityCode": addresses[indexOfRegion].cityCode ?? "", "CountyCode": addresses[indexOfRegion].countyCode ?? "", "DistrictCode": addresses[indexOfRegion].districtCode ?? "", "Apartment": apartmentTextField.text ?? "", "DoorNumber": doorNumberTextField.text ?? "", "Instructions": additionalInstructions.text ?? "", "Phone": user.phone, "StreetName": streetTextField.text ?? "", "RoadName": ""]
            
            let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]
            
            print(parameters)
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(json)

                        // update address in myAddresses
                        downloadMyAddresses()

                        let alert = UIAlertController(title: "", message: json["data"]["message"].string, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            var viewControllers = self.navigationController?.viewControllers
                            viewControllers?.removeLast(3)
                            self.navigationController?.setViewControllers(viewControllers!, animated: true)
                        })
                        
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func localize() {
        
    }
    
    func adjust(){
        
    }
    
    @IBAction func textTapped(_ sender: UITextField) {
        selectedTextFieldTag = sender.tag
        picker.reloadAllComponents()
    }
    
    @objc func addressDownloaded() {
        picker.reloadAllComponents()

        guard indexOfAddress != nil else {return}

        if !isAddingNew {
            for i in 0 ..< addresses.count {
                if addresses[i].cityCode! == myAddresses[indexOfAddress!].cityCode {
                    picker.selectRow(i, inComponent: 0, animated: true)
                }
            }

            for i in 0 ..< addresses.count {
                if addresses[i].districtCode! == myAddresses[indexOfAddress!].dictrictCode {
                    picker.selectRow(i, inComponent: 0, animated: true)
                }
            }

            regionTextField.text = myAddresses[indexOfAddress!].regionName
            streetTextField.text = myAddresses[indexOfAddress!].streetName
            apartmentTextField.text = myAddresses[indexOfAddress!].apartment
            doorNumberTextField.text = myAddresses[indexOfAddress!].doorNumber
            additionalInstructions.text = myAddresses[indexOfAddress!].instructions
        }

    }
    
    func createDatePicker() {
        // toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for toolbar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        regionTextField.inputAccessoryView = toolbar
        regionTextField.inputView = picker
        
        streetTextField.inputAccessoryView = toolbar
        streetTextField.inputView = picker
        
        picker.delegate = self
        picker.dataSource = self
    }
    
    @objc func donePressed() {
        self.view.endEditing(true)
    }
    
}

extension AddressInfoController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedTextFieldTag == 0 {
            return addresses.count
        } else if selectedTextFieldTag == 1 && selectedIndexOfRegion != -1 {
            guard let index = selectedIndexOfRegion else {return 0}
            return addresses[index].streets!.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedTextFieldTag == 0 {
            selectedIndexOfRegion = row
            
            regionTextField.text = addresses[row].regionName
            selectedIndexOfStreet = -1
            streetTextField.text = ""
            
            streetTextField.isEnabled = true
        } else if selectedTextFieldTag == 1 && selectedIndexOfRegion != -1{
            selectedIndexOfStreet = row
            guard let index = selectedIndexOfRegion else {return}
            streetTextField.text = addresses[index].streets![row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selectedTextFieldTag == 0 {
            guard let name = addresses[row].regionName else { return "Salam" }
            return name
        } else if selectedTextFieldTag == 1 {
            guard let index = selectedIndexOfRegion else {return ""}
            guard let name = addresses[index].streets![row].name else { return "Salam" }
            return name
        }
        return ""
    }
    
}





