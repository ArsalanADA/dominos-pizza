//
//  ProductsCollectionController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 01.12.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

private let reuseIdentifier = "productCell"
private let reuseIdentifierGrid = "productCellGrid"

class ProductsCollectionController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var layoutButton: UIBarButtonItem!
    
    var gridLayout: GridLayout!
    var listLayout: ListLayout!
    
    var categoryCodeCurrent: String = ""
    
    var customTitle: String = ""
    var isPizzaSelected: Bool = false
    var isSelectingProduct: Bool = false
    var showOnlyFavorite: Bool = false
    
    var twoDimensionalArray: [[MODEL]] = [
        [],
        [],
        []
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if model.isEmpty {
            activityIndicator.startAnimating()
        }
        
        create2dArray()
        
        print("Show only favorite:", showOnlyFavorite)
        
        self.title = customTitle
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .reloadProducts, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .languageChanged, object: nil)
        
        gridLayout = GridLayout(numberOfColumns: 2)
        listLayout = ListLayout()
        
        collectionView.collectionViewLayout = listLayout
        collectionView.reloadData()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        downloadImagesOfModels()
    }
    
    func create2dArray() {
        twoDimensionalArray = [
            [],[],[]
        ]
        
        if isPizzaSelected {
            for i in 0 ..< model.count {
                if i == 0 {
                    twoDimensionalArray[0].append(model[i])
                } else {
                    if model[i].isSpecial {
                        twoDimensionalArray[1].append(model[i])
                    } else {
                        twoDimensionalArray[2].append(model[i])
                    }
                }
            }
        } else {
            for m in model {
                twoDimensionalArray[0].append(m)
            }
        }
    }
    
    @objc func reload() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.collectionView.reloadData()
        }
    }

    @IBAction func changeLayout(_ sender: UIBarButtonItem) {
        self.collectionView.collectionViewLayout.invalidateLayout()
        if collectionView.collectionViewLayout == gridLayout {
            layoutButton.image = #imageLiteral(resourceName: "gridIcon")
            self.collectionView.setCollectionViewLayout(self.listLayout, animated: false)
        } else {
            layoutButton.image = #imageLiteral(resourceName: "listIcon")
            self.collectionView.setCollectionViewLayout(self.gridLayout, animated: false)
        }
        self.collectionView.reloadData()
        let index = IndexPath(row: 0, section: 0)
        self.collectionView.scrollToItem(at: index, at: .top, animated: false)
    }
    
    @IBAction func pp(_ sender: UIButton) {
        model[sender.tag].food.isFavorite = !model[sender.tag].food.isFavorite
        self.collectionView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MelumatController {
            destination.isPizzaSelected = isPizzaSelected
            destination.isSelecting = isSelectingProduct
        }
    }

    func downloadImagesOfModels() {
        DispatchQueue.global(qos: .userInteractive).async {
            for m in model {
                if m.food.image == nil {
                    var url = URL(string: "http://www.dominospizza.az/rest/medium/Product/Detail/\(m.food.productCode)/Resize/224x224/AZ/View")
                    if self.isPizzaSelected {
                        url = URL(string: "http://www.dominospizza.az/rest/medium/ProductOsg/Detail/\(m.food.productCode)/Resize/224x224/AZ/view")
                    }
                    getData(from: url!) { (data, response, error) in
                        if let image = UIImage(data: data!) {
                            for i in 0 ..< model.count {
                                if model[i].food.productCode == m.food.productCode {
                                    model[i].food.image = image
                                    DispatchQueue.main.async {
                                        self.collectionView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension ProductsCollectionController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return twoDimensionalArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return showOnlyFavorite ? twoDimensionalArray[section].filter({return $0.food.isFavorite}).count : twoDimensionalArray[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        foodObject = twoDimensionalArray[indexPath.section][indexPath.row].food
        performSegue(withIdentifier: "pizzaMelimatiSegue", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if showOnlyFavorite {
            model = model.filter({return $0.food.isFavorite})
        }
        
        if collectionView.collectionViewLayout == listLayout {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProductCell
            cell.productImageView.image = twoDimensionalArray[indexPath.section][indexPath.row].food.image
            cell.productTitleLabel.text = twoDimensionalArray[indexPath.section][indexPath.row].food.name

            // If pizza is selected show ingredients else price
            if isPizzaSelected {
                cell.productIngredientsLabel.text = flatArrayOfIngredients(twoDimensionalArray[indexPath.section][indexPath.row].ingredients)
            } else {
                cell.productIngredientsLabel.attributedText = convertToPrice(price: twoDimensionalArray[indexPath.section][indexPath.row].food.surchargelessPrice)
            }

            cell.likeButton.tag = indexPath.row

            if isPizzaSelected {
                cell.likeButton.isHidden = false
            } else {
                cell.likeButton.isHidden = true
            }

            if twoDimensionalArray[indexPath.section][indexPath.row].food.isFavorite {
                cell.likeButton.setBackgroundImage(#imageLiteral(resourceName: "likeIconFilled"), for: .normal)
            } else {
                cell.likeButton.setBackgroundImage(#imageLiteral(resourceName: "likeIcon"), for: .normal)
            }

            // Wrong but goes
            cell.isFavorite = twoDimensionalArray[indexPath.section][indexPath.row].food.isFavorite
            cell.productCode = twoDimensionalArray[indexPath.section][indexPath.row].food.productCode

            return cell
        } else {                    // grid layout
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierGrid, for: indexPath) as! productCellGrid

            cell.productImageView.image = twoDimensionalArray[indexPath.section][indexPath.row].food.image
            cell.productTitleLabel.text = twoDimensionalArray[indexPath.section][indexPath.row].food.name

            // If pizza is selected show ingredients else price
            if isPizzaSelected {
                cell.productIngredientsLabel.text = flatArrayOfIngredients(twoDimensionalArray[indexPath.section][indexPath.row].ingredients)
            } else {
                cell.productIngredientsLabel.attributedText = convertToPrice(price: twoDimensionalArray[indexPath.section][indexPath.row].food.surchargelessPrice)
            }

            cell.likeButton.tag = indexPath.row

            if isPizzaSelected {
                cell.likeButton.isHidden = false
            } else {
                cell.likeButton.isHidden = true
            }

            if twoDimensionalArray[indexPath.section][indexPath.row].food.isFavorite {
                cell.likeButton.setBackgroundImage(#imageLiteral(resourceName: "likeIconFilled"), for: .normal)
            } else {
                cell.likeButton.setBackgroundImage(#imageLiteral(resourceName: "likeIcon"), for: .normal)
            }

            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderView", for: indexPath) as! SectionHeaderView
        
        switch indexPath.section {
        case 0:
            sectionView.titleCustom = "make your own pizza"
        case 1:
            sectionView.titleCustom = "Special".localized()
        case 2:
            sectionView.titleCustom = "Favorite".localized()
        default:
            sectionView.titleCustom = "no name"
        }
        
        if isPizzaSelected {
            sectionView.isHidden = false
        } else {
            sectionView.isHidden = true
        }
        
        if indexPath.section == 0 {
            sectionView.isHidden = true
        }
        
        return sectionView
    }
    
}


