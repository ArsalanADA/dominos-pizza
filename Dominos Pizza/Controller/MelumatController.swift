//
//  MelumatController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 01.12.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Localize_Swift

var foodObject: Food?

class MelumatController: UIViewController {
    
    var url: String = ""
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var messageLabel: UILabel!

    var isChanging: Bool? = false
    var isPizzaSelected: Bool = false
    var isSelecting: Bool = false

    var quantityFromOrder: Double = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = foodObject?.name
        orderQuantityFromStepper = 1

        if isChanging ?? false {
            orderQuantityFromStepper = quantityFromOrder
        }

        layout()
        addObservers()
        addNumberOfOrdersBadge()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if isPizzaSelected && !isChanging! {
            downloadObjectDetails()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: .pizzaAddDeleted, object: nil)
    }

    func layout() {
        messageLabel.text = "added".localized()
        messageLabel.adjustsFontSizeToFitWidth = true
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(addToOrder(_:)), name: .newOrderAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .detailsDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .doughTypeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(newProductAdded), name: .newProductAdded, object: nil)
    }
    
    @objc func newProductAdded() {
        var viewControllers = self.navigationController?.viewControllers
        viewControllers?.removeLast(2) //here 2 views to pop index numbers of views
        self.navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    
    @objc func addNumberOfOrdersBadge() {
        // badge label
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: 20, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.textColor = .white
        label.backgroundColor = .red
        label.text = "\(orders.count)"
        
        if orders.count == 0 {
            label.isHidden = true
        } else {
            label.isHidden = false
        }
        
        label.adjustsFontSizeToFitWidth = true
        
        // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage( #imageLiteral(resourceName: "cartIcon") , for: .normal)
        rightButton.addTarget(self, action: #selector(showBasket), for: .touchUpInside)
        rightButton.addSubview(label)
        
        // Bar button item
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtomItem
    }
    
    @objc func showBasket() {
        performSegue(withIdentifier: "showBasket", sender: self)
    }
    
    @objc func sizeChanged() {
        resetAll()
    }

    func resetAll() {
        guard let pizza = foodObject as? AbstractPizza else {return}
        
        //reset dough
        for i in 0 ..< pizza.pizzas[pizza.selectedSize.rawValue].dough.count {
            pizza.pizzas[pizza.selectedSize.rawValue].dough[i].isSelected = false
        }
        
        //reset sides
        for i in 0 ..< pizza.pizzas[pizza.selectedSize.rawValue].rim.count {
            pizza.pizzas[pizza.selectedSize.rawValue].rim[i].isSelected = false
        }
        
        // select first ones
        pizza.pizzas[pizza.selectedSize.rawValue].dough[0].isSelected = true
        pizza.pizzas[pizza.selectedSize.rawValue].rim[0].isSelected = true
    }
    
    @objc func reload() {

        // FIXME: ????
        if isPizzaSelected {
            let index = IndexPath(row: 1, section: 0)
            DispatchQueue.main.async {
                self.tableView.scrollToRow(at: index, at: .none, animated: false)
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        NotificationCenter.default.post(name: .reload22, object: nil)
    }

    @IBAction func ok() {
        
    }
    
    @objc func addToOrder(_ notification: NSNotification) {
        
        var point: CGPoint = CGPoint(x: 550, y: 0)
        
        if let cgPoint: CGPoint = notification.userInfo?["cgpoint"] as? CGPoint {
            point = cgPoint
        }
        
        if isPizzaSelected {
            guard let pizza = foodObject as? AbstractPizza else {return}
            addToOrderList(food: pizza, quantity: Int(orderQuantityFromStepper))
        } else {
            guard foodObject != nil else {return}
            addToOrderList(food: foodObject!, quantity: Int(orderQuantityFromStepper))
        }
        
        let view: UIImageView = UIImageView(image: foodObject?.image)

        animate(view: view, fromPoint: point, toPoint: CGPoint.init(x: UIScreen.main.bounds.size.width - 20, y: -10))
    }
    
    func animate(view: UIView, fromPoint start: CGPoint, toPoint end: CGPoint) {

        let view: UIImageView = UIImageView(image: foodObject?.image ?? UIImage())
        view.frame = CGRect(x: 55, y: 300, width: 20, height: 20)
        view.layer.cornerRadius = view.frame.size.height / 2.0
        view.clipsToBounds = true

        self.view.addSubview(view)

        let path = UIBezierPath()

        path.move(to: start)

        let controlPoint1 = CGPoint(x: start.x + 20, y: start.y - 100)
        let controlPoint2 = CGPoint(x: start.x + 50, y: start.y - 200)

        path.addCurve(to: end, controlPoint1: controlPoint1, controlPoint2: controlPoint2)

        // create the animation
        let animation = CAKeyframeAnimation(keyPath: "position")
        animation.path = path.cgPath
        animation.rotationMode = kCAAnimationRotateAuto

        // to complete one animation
        animation.duration = 1

        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards

        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseInOut], animations: {
            view.layer.position = end
        }) { (done) in
            self.addNumberOfOrdersBadge()
            var viewControllers = self.navigationController?.viewControllers
            viewControllers?.removeLast(2) //here 2 views to pop index numbers of views
            self.navigationController?.setViewControllers(viewControllers!, animated: true)
        }
    }

    func downloadObjectDetails() {
        let url = baseURL + "product/\(foodObject?.productCode ?? "noCode")?language=\(Localize.currentLanguage())"
        Alamofire.request(url).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    _ = parse2PizzaO(JSONData: json)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension MelumatController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if foodObject is AbstractPizza {
            return 9
        }
        return 3
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        // Show rims only if classic dough is selected
        if let pizza = foodObject as? AbstractPizza {
            if pizza.pizzas[pizza.selectedSize.rawValue].selectedDough()?.code != "HANDTOSS" {
                rimCC = 0.0
            }
        }

        switch indexPath.row {
        case 0: return 250
        case 1: return 50
        case 2: return 100
        case 3: return 250
        case 4: return doughCC
        case 5: return rimCC
        case 6: return exToppings
        case 7: return stToppings
        default: return 200
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var imageCell = ImageCell()
        var amountCell = AmountCell()
        var descriptionCell = DescriptionCell()
        var sizeCell = SizeCell()
        var doughCell = DoughCell()
        var rimCell = RimCell()
        var standartToppingCell = StandartToppingsCell()
        var extraToppingCell = ExtraToppingCell()
        var sebeteElaveCell = SebeteElaveEtCell()

        if isPizzaSelected {
            switch indexPath.row {
            case 0:
                imageCell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! ImageCell
                imageCell.image2 = foodObject?.image
                return imageCell
                
            case 1:
                amountCell = tableView.dequeueReusableCell(withIdentifier: "amount", for: indexPath) as! AmountCell
                amountCell.isPizzaSelected = isPizzaSelected
                amountCell.isChanging = isChanging
                amountCell.isSelecting = isSelecting
                return amountCell
                
            case 2:
                descriptionCell = tableView.dequeueReusableCell(withIdentifier: "description", for: indexPath) as! DescriptionCell
                return descriptionCell
                
            case 3:
                sizeCell = tableView.dequeueReusableCell(withIdentifier: "size", for: indexPath) as! SizeCell
                sizeCell.isSelecting = isSelecting
                return sizeCell
                
            case 4:
                doughCell = tableView.dequeueReusableCell(withIdentifier: "dough", for: indexPath) as! DoughCell
                return doughCell
                
            case 5:
                rimCell = tableView.dequeueReusableCell(withIdentifier: "rim", for: indexPath) as! RimCell
                return rimCell
                
            case 6:
                extraToppingCell = tableView.dequeueReusableCell(withIdentifier: "extraToppings", for: indexPath) as! ExtraToppingCell
                return extraToppingCell
                
            case 7:
                standartToppingCell = tableView.dequeueReusableCell(withIdentifier: "standartToppings", for: indexPath) as! StandartToppingsCell
                return standartToppingCell
                
            case 8:
                sebeteElaveCell = tableView.dequeueReusableCell(withIdentifier: "sebete", for: indexPath) as! SebeteElaveEtCell
                sebeteElaveCell.isChanging = isChanging!
                sebeteElaveCell.isProductSelecting = isSelecting
                return sebeteElaveCell
                
            default:
                return UITableViewCell()
            }
        } else {
            switch indexPath.row {
            case 0:
                imageCell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! ImageCell
                imageCell.image2 = foodObject?.image
                return imageCell
                
            case 1:
                amountCell = tableView.dequeueReusableCell(withIdentifier: "amount", for: indexPath) as! AmountCell
                amountCell.isPizzaSelected = isPizzaSelected
                return amountCell
                
            case 2:
                sebeteElaveCell = tableView.dequeueReusableCell(withIdentifier: "sebete", for: indexPath) as! SebeteElaveEtCell
                sebeteElaveCell.isChanging = isChanging!
                sebeteElaveCell.isProductSelecting = isSelecting
                return sebeteElaveCell
                
            default:
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertToOptionalCAAnimationRotationMode(_ input: String?) -> CAAnimationRotationMode? {
//    guard let input = input else { return nil }
//    return CAAnimationRotationMode(rawValue: input)
//}
//
//// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertFromCAAnimationRotationMode(_ input: CAAnimationRotationMode) -> String {
//    return input.rawValue
//}
//
//// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertToCAMediaTimingFillMode(_ input: String) -> CAMediaTimingFillMode {
//    return CAMediaTimingFillMode(rawValue: input)
//}
//
//// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertFromCAMediaTimingFillMode(_ input: CAMediaTimingFillMode) -> String {
//    return input.rawValue
//}
