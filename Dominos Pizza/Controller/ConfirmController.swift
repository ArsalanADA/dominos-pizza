//
//  ConfirmController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 21.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class ConfirmController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear

        NotificationCenter.default.addObserver(self, selector: #selector(orderSubmitted(_:)), name: .orderSubmitted, object: nil)
    }

    @objc func orderSubmitted(_ notification: Notification) {

        guard let response = notification.userInfo!["response"] as? [String: Any] else {return}

        // Message
        if let message = response["message"] as? String {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                // Tracking code
                if let trackingCode = response["trackingCode"] as? String {
                    UserDefaults.standard.setValue(trackingCode, forKey: trackingCodeKey)
                    let alert = UIAlertController(title: nil, message: "Tracking code: " + trackingCode, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        var viewControllers = self.navigationController?.viewControllers
                        viewControllers?.removeLast(2)
                        self.navigationController?.setViewControllers(viewControllers!, animated: true)

                        // Clean cart
                        orders.removeAll()
                    }
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "confirmCell", for: indexPath) as! ConfirmCell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name("touched"), object: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1600
    }

    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}





