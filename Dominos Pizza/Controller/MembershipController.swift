//
//  MembershipController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MembershipController: UIViewController {
    
    
    @IBOutlet weak var firstnameLabel: UILabel!
    @IBOutlet weak var lastnameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var firstnameTextField: UITextField!
    @IBOutlet var lastnameTextField: UITextField!
    
    @IBOutlet weak var phoneCodeTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!

    let picker = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()

        placeOldValues()
        createDatePicker()
        localize()
    }

    func localize() {
        firstnameLabel.text = "firstname".localized()
        lastnameLabel.text = "lastname".localized()
        phoneLabel.text = "phone".localized()
        emailLabel.text = "email".localized()
    }
    
    func createDatePicker() {

        // toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()

        // done button for toolbar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)

        phoneCodeTextField.inputAccessoryView = toolbar
        phoneCodeTextField.inputView = picker

        picker.delegate = self
        picker.dataSource = self
    }

    @objc func donePressed() {
        self.view.endEditing(true)
    }

    func placeOldValues() {
        guard let user = currentUser() else {return}

        firstnameTextField.text = user.firstname
        lastnameTextField.text = user.lastname
        emailTextField.text = user.email
        
        let phone: String = user.phone
        let index = phone.index(phone.startIndex, offsetBy: 5)
        let index2 = phone.index(phone.startIndex, offsetBy: 6)
        let code: String = String(phone[...index])
        
        phoneCodeTextField.text = code
        print(String(phone[index2...]))
        phoneTextField.text = String(phone[index2...])
    }
    
    @IBAction func save() {
        activityIndicator.startAnimating()
        
        let url = baseURL + "user/update"
        
        let firstname = firstnameTextField.text
        let lastname = lastnameTextField.text
        let phone = (phoneCodeTextField.text ?? "") + (phoneTextField.text ?? "")
        let email = emailTextField.text
        
        guard let user = currentUser() else {return}
        
        let parameters: Parameters = ["firstname": firstname ?? "", "lastname": lastname ?? "", "phone": phone , "email": email ?? "", "sex": "0", "birthdate": "123"]
        let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)" ]
        
        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.httpBody, headers: header).validate().responseJSON { (response) in
            self.activityIndicator.stopAnimating()
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    let user: User = User(firstname: json["data"]["userData"]["firstname"].string ?? "NO NAME", lastname: json["data"]["userData"]["lastname"].string ?? "NO LAstname", email: json["data"]["userData"]["email"].string ?? "no email", sex: .male, phone: json["data"]["userData"]["phone"].string ?? "No phone", token: json["data"]["userData"]["token"].string ?? "NO token", id: json["data"]["userData"]["id"].string ?? "no id", birthdate: json["data"]["userData"]["birthdate"].int)
                    
                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
                    UserDefaults.standard.set(encodedData, forKey: userKeyString)
                    NotificationCenter.default.post(name: .membershipInfoChanged, object: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}



extension MembershipController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numberCodes.count
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        phoneCodeTextField.text = numberCodes[row]
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return numberCodes[row]
    }

}




