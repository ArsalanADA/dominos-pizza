//
//  ChangePasswordViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 18.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func saveChanges() {
        activityIndicator.startAnimating()
        
        let url = baseURL + "user/change-password"
        
        let oldPassword = oldPasswordTextField.text
        let newPassword = newPasswordTextField.text
        let confirmPassword = confirmTextField.text
        
        guard let user = currentUser() else {return}
        
        let parameters: Parameters = ["oldPassword": oldPassword ?? "", "newPassword": newPassword ?? "", "confirmPassword": confirmPassword ?? ""]
        let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)" ]
        
        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response) in
            self.activityIndicator.stopAnimating()
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    print(json["data"])
                    
                    let success = json["data"]["success"].int
                    let message = json["data"]["message"].string
                    
                    print(message ?? "no message")
                    
                    if success == 0 {
                        let alert = UIAlertController(title: "Problem", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        
                        let alert = UIAlertController(title: "Saved", message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            case .failure: break
            }
        }
    }
}
