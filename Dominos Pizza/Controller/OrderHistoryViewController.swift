//
//  OrderHistoryViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.05.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

var myOrders: [HistoryOrder] = []

class OrderHistoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .orderHistoryDownloaded, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        downloadHistoryOrders()
    }

    @objc func reload() {
        self.tableView.reloadData()
    }

}

extension OrderHistoryViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myOrders.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderHistoryCell", for: indexPath) as! OrderHistoryTableViewCell

        cell.idLabel.text = myOrders[indexPath.row].id
        cell.storeLabel.text = myOrders[indexPath.row].store
        cell.dateLabel.text = myOrders[indexPath.row].date
        cell.priceLabel.attributedText = convertToPrice(price: myOrders[indexPath.row].price, color: .white)

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }

}













