//
//  SideMenuVC.swift
//
//  Created by Arsalan Iravani on 09.01.2018.
//

import UIKit
import Localize_Swift
import Foundation
import ObjectiveC
import CoreFoundation
import AVFoundation

let instagram = "https://www.instagram.com/dominos_aze"
let facebook = "https://www.facebook.com/Dominos.Pizza.Azerbaijan/"

class SideMenuVC: UIViewController {
    
    @IBOutlet var nameLable: UILabel!
    @IBOutlet var helloLabel: UILabel!
    
    @IBOutlet var azLineView: UIView!
    @IBOutlet var enLineView: UIView!
    
    override func viewDidLoad() {
        
        helloLabel.adjustsFontSizeToFitWidth = true
        
        localize()
        userCh()
        
        NotificationCenter.default.addObserver(self, selector: #selector(userCh), name: .userChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(localize), name: .languageChanged, object: nil)
    }
    
    @objc func localize() {
        helloLabel.text = "hello".localized()
        
        switch Localize.currentLanguage() {
        case "en":
            enLineView.isHidden = false
            azLineView.isHidden = true
            
        case "az":
            enLineView.isHidden = true
            azLineView.isHidden = false
        case "ru":
            enLineView.isHidden = true
            azLineView.isHidden = true
        default: break
        }
    }
    
    @objc func userCh() {
        if let user = currentUser() {
            helloLabel.isHidden = false
            nameLable.isHidden = false
            nameLable.text = user.firstname + " " + user.lastname
        } else {
            helloLabel.isHidden = true
            nameLable.isHidden = true
        }
    }
    
    @IBAction func changeToAzeri() {
        Localize.setCurrentLanguage("az")
        reload()
    }
    
    @IBAction func changeToEnglish() {
        Localize.setCurrentLanguage("en")
        reload()
    }
    
    func reload() {
        NotificationCenter.default.post(name: .languageChanged, object: nil)
        downloadAllProducts()
    }
    
    @IBAction func openSocialMedia(_ sender: UIButton) {
        var url = URL(string: facebook)
        
        if sender.tag == 1 {
            url = URL(string: instagram)
        }
        
        guard url != nil else { return }
        
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
}


