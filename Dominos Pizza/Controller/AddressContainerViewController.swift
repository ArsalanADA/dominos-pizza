//
//  AddressContainerViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

var isAddingNew: Bool = false

class AddressContainerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(t))
        
        NotificationCenter.default.addObserver(self, selector: #selector(showAddress(_:)), name: .addressSelected, object: nil)
    }

    var index = -1

    @objc func showAddress(_ notification: Notification) {
        if let index: Int = notification.userInfo!["index"] as? Int {
            self.index = index
            self.performSegue(withIdentifier: "addAddress", sender: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? AddressInfoController {
            destination.indexOfAddress = self.index
        }
    }

    @objc func t() {
        self.performSegue(withIdentifier: "addAddress", sender: nil)
        isAddingNew = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
