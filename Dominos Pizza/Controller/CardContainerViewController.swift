//
//  CardContainerViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 15.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class CardContainerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        NotificationCenter.default.addObserver(self, selector: #selector(checkoutPressed), name: .checkoutPressed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(suggestionPressed), name: .suggestionPressed, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(phoneEditPressed), name: .addressEditPressed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(phoneEditPressed), name: .phoneEditPressed, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(orderChangePressed), name: .orderChangePressed, object: nil)
    }

    var orderObjectToBeChanged: Order?

    @objc func orderChangePressed(_ notification: Notification) {
            if let object = notification.userInfo!["orderObject"] as? Order {
                orderObjectToBeChanged = object
            }
            self.performSegue(withIdentifier: "showMelumateController", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MelumatController {
            destination.isChanging = true
            destination.isPizzaSelected = true
            destination.quantityFromOrder = Double((orderObjectToBeChanged?.quantity)!)
            foodObject = orderObjectToBeChanged?.food as! AbstractPizza
        }
    }

    @objc func phoneEditPressed() {
        self.performSegue(withIdentifier: "phoneEditPressed", sender: nil)
    }

    @objc func suggestionPressed() {
        self.performSegue(withIdentifier: "showSuggestionProducts", sender: nil)
    }

    @objc func checkoutPressed() {
        if let data = UserDefaults.standard.data(forKey: userKeyString), let _ = NSKeyedUnarchiver.unarchiveObject(with: data) as? User {
            if !myAddresses.isEmpty {
                self.performSegue(withIdentifier: "showCheckout", sender: nil)
            } else {
                // TODO: Check if user tapped carry out
                let alert = UIAlertController(title: "Please add an address", message: nil, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.performSegue(withIdentifier: "phoneEditPressed", sender: nil)
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Please log in or sign up to proceed", message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
