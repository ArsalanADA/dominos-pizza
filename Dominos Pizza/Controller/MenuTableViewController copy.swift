//
//  MenuTableViewController.swift
//  
//
//  Created by Arsalan Iravani on 08.02.2018.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    let ifLogged: [(icon: UIImage, title: String)] = [(#imageLiteral(resourceName: "userIcon"), "Membership Info"), (#imageLiteral(resourceName: "likeIcon"), "Favorites"), (#imageLiteral(resourceName: "orders2"), "Orders"), (#imageLiteral(resourceName: "loginIcon"), "Log out")] 
    
    let ifNotLogged: [(icon: UIImage, title: String)] = [(#imageLiteral(resourceName: "loginIcon"), "Log in"), (#imageLiteral(resourceName: "loginIcon"), "Sign up")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .userChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .languageChanged, object: nil)
    }
    
    @objc func reload() {
        self.tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.value(forKey: userKeyString) != nil {
            return ifLogged.count
        } else {
            return ifNotLogged.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableCell", for: indexPath) as! MenuTableViewCell

        if UserDefaults.standard.value(forKey: userKeyString) != nil {
            cell.iconImageView.image = ifLogged[indexPath.row].icon
            cell.titleLabel.text = ifLogged[indexPath.row].title.localized()
        } else {
            cell.iconImageView.image = ifNotLogged[indexPath.row].icon
            cell.titleLabel.text = ifNotLogged[indexPath.row].title.localized()
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserDefaults.standard.value(forKey: userKeyString) != nil {
            switch indexPath.row {
            case 0: NotificationCenter.default.post(name: .membershipInfoPressed, object: nil)
            case 1: NotificationCenter.default.post(name: .favoritesPressed, object: nil)
            case 2: NotificationCenter.default.post(name: .ordersPressed, object: nil)
            case 3:
                UserDefaults.standard.removeObject(forKey: userKeyString)
                NotificationCenter.default.post(name: .userChanged, object: nil)
                self.tableView.reloadData()
            default:
                break
            }
        } else {
            switch indexPath.row {
            case 0: NotificationCenter.default.post(name: .loginPressed, object: nil)
            case 1: NotificationCenter.default.post(name: .registerPressed, object: nil)
            default:
                break
            }
        }
    }
    
}
