//
//  ViewController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 27.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights  reserved.
//

import UIKit
import Localize_Swift

var isOpen: Bool = false

class Home: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var customTitleView: UIView!

    let menu: [MenuButton] = [MenuButton(title: "Delivery", subtitle: "", image: #imageLiteral(resourceName: "menu 2")), MenuButton(title: "Carry Out", subtitle: "carryS", image: #imageLiteral(resourceName: "carryOut")), MenuButton(title: "Branches", subtitle: "storeS", image: #imageLiteral(resourceName: "menu 4")), MenuButton(title: "Pizza Tracker".localized(), subtitle: "trackerS".localized(), image: #imageLiteral(resourceName: "pizza_tracker"))]
    
    override func viewDidLoad() {
        collectionView.backgroundColor = .clear
        
        isOpen = false
        
        self.navigationItem.titleView = customTitleView
        NotificationCenter.default.addObserver(self, selector: #selector(showMenu(_:)), name: .toggleMenu, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .languageChanged, object: nil)
    }
    
    @objc func reload() {
        self.collectionView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        reload()
    }
    
    @objc func showMenu(_ sender: UIBarButtonItem) {

        if !isOpen {
            sideMenuViewController?._presentLeftMenuViewController()
        } else {
            sideMenuViewController?.hideMenuViewController()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCell", for: indexPath) as! HomeCell
        cell.imageView.image = menu[indexPath.row].image
        cell.titleLabel.text = NSLocalizedString(menu[indexPath.row].title.localized(), comment: "")
        cell.subtitleLabel.text = NSLocalizedString(menu[indexPath.row].subtitle.localized(), comment: "")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        switch indexPath.row {
        case 0:
            self.tabBarController?.selectedIndex = 1
            delivaryMethod = .delivery
            NotificationCenter.default.post(name: .delivaryMethodChanged, object: nil)
            if let arrayOfTabBarItems = super.tabBarController?.tabBar.items {
                let tabBarItem = arrayOfTabBarItems[1]
                tabBarItem.isEnabled = true
            }
        case 1:
            self.tabBarController?.selectedIndex = 1
            delivaryMethod = .carryOut
            NotificationCenter.default.post(name: .delivaryMethodChanged, object: nil)
            if let arrayOfTabBarItems = super.tabBarController?.tabBar.items {
                let tabBarItem = arrayOfTabBarItems[1]
                tabBarItem.isEnabled = true
            }
        case 2:
            performSegue(withIdentifier: "store", sender: self)
        case 3:
            performSegue(withIdentifier: "tracker", sender: self)

        default:
            break
        }
        
    }
    
    @IBAction func call() {
        if let url = URL(string: "tel://*6600"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
//    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
//}
