//
//  StoreLocator.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 01.12.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

var stores: [Store] = []
var markers: [GMSMarker] = []

var nearestStore: Store?

let authStatus = CLLocationManager.authorizationStatus()
let inUse = CLAuthorizationStatus.authorizedWhenInUse
let always = CLAuthorizationStatus.authorizedAlways

var currentLocation: CLLocationCoordinate2D?

let zoomLevel: Float = 11.0

class StoreLocator: UIViewController, LocateOnTheMap {
    
    @IBOutlet var infoView: InfoView!
    @IBOutlet var nearestButton: UIButtonX!

    var storesAddresses = [String: String]()
    var locationManager = CLLocationManager()

    let mapView: GMSMapView = {
        let camera = GMSCameraPosition.camera(withLatitude: 40.40511065, longitude:  49.83777855, zoom: zoomLevel)
        let m = GMSMapView.map(withFrame: .zero, camera: camera)
        m.accessibilityElementsHidden = false
        m.isMyLocationEnabled = true
        m.translatesAutoresizingMaskIntoConstraints = false
        m.settings.compassButton = true
        m.settings.myLocationButton = true
        return m
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        mapView.delegate = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        self.nearestButton.isEnabled = false

        nearestButton.setTitle("storeS".localized(), for: .normal)
        
        layout()
        downloadStoresAndPlaceMarkers()

        storesAddresses["97405"] = "M.Müşviq küç., 9A (N.Nərimanov heykəlinin arxası)"
        storesAddresses["97406"] = "Azadlıq pr., 3 (Hökümət evinin yanı)"
        storesAddresses["97407"] = "Koroğlu, 3 (Müəllimlər İnstitutu ilə üzbəüz)"
        storesAddresses["97408"] = "M.Hadi küç., 142 (H.Aslanov m/s çıxışı)"
        storesAddresses["97409"] = "Q.Qarayev pr. 34 S, (Q.Qarayev m/s çıxışı)"
    }

    func location() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 2
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }

    /**
     Locate map with longitude and longitude after search location on UISearchBar
     
     - parameter lon:   longitude location
     - parameter lat:   latitude location
     - parameter title: title of address location
     */
    func locateWith(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        DispatchQueue.main.async {
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
            self.mapView.camera = camera
        }
    }
    
    /**
     Searchbar when text change
     
     - parameter searchBar:  searchbar UI
     - parameter searchText: searchtext description
     */
    
    func layout() {
        
        self.view.addSubview(mapView)
        self.mapView.addSubview(infoView)

        self.view.bringSubview(toFront: nearestButton)

        infoView.translatesAutoresizingMaskIntoConstraints = false
        
        mapView.leadingAnchor.constraint(equalTo: super.view.leadingAnchor, constant: 0).isActive = true
        mapView.trailingAnchor.constraint(equalTo: super.view.trailingAnchor, constant: 0).isActive = true
        mapView.topAnchor.constraint(equalTo: super.view.topAnchor, constant: 0).isActive = true
        mapView.bottomAnchor.constraint(equalTo: super.view.bottomAnchor, constant: 0).isActive = true
        
        infoView.leadingAnchor.constraint(equalTo: self.mapView.leadingAnchor).isActive = true
        infoView.trailingAnchor.constraint(equalTo: self.mapView.trailingAnchor).isActive = true
        infoView.topAnchor.constraint(equalTo: self.mapView.bottomAnchor, constant: -150).isActive = true
        infoView.bottomAnchor.constraint(equalTo: self.mapView.bottomAnchor).isActive = true
    }

    /// Creates a marker in the map.
    func downloadStoresAndPlaceMarkers() {
        
        let stringURL = baseURL + "stores"
        stores.removeAll()
        markers.removeAll()

        Alamofire.request(stringURL).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let storesJSON = json["data"]["stores"]
                    
                    for (_, subJson) in storesJSON {
                        guard
                            let locationCode = subJson["LocationCode"].string,
                            let name = subJson["Name"].string,
                            let IPAddress = subJson["IPAddress"].string,
                            let availableFrom = subJson["AvailableFrom"].string,
                            let availableUntil = subJson["AvailableUntil"].string
                            else { return }
                        
                        let Longitude = subJson["Longitude"].doubleValue
                        let Lattitude = subJson["Lattitude"].doubleValue

                        DispatchQueue.main.async() {
                            let store = Store(locationCode: locationCode, name: name, address: self.storesAddresses[locationCode] ?? "", IPAddress: IPAddress, longitude: Longitude, lattitude: Lattitude, availableFrom: availableFrom, availableUntil: availableUntil)
                            
                            stores.append(store)
                            
                            let marker = GMSMarker()
                            marker.position = CLLocationCoordinate2D(latitude: Lattitude, longitude: Longitude)
                            marker.icon = #imageLiteral(resourceName: "logo")
                            marker.title = name
                            // marker.snippet = "Your Favorite Store"
                            marker.appearAnimation = .pop
                            marker.map = self.mapView

                            if CLLocationManager.locationServicesEnabled() {
                                self.nearestButton.isEnabled = true
                            }
                        }
                    }
                }
            case .failure(let error):
                print("Error \n",error)
                return
            }
        }
    }
    
    @IBAction func findNearestStore() {

        guard !stores.isEmpty else {return}

        guard currentLocation != nil else {return}

        // Initialize with default values
        var minDistance: Double = GMSGeometryDistance(currentLocation!, CLLocationCoordinate2D(latitude: stores[0].lattitude, longitude:  stores[0].longitude))
        nearestStore = stores[0]

        for store in stores {
            let distance = GMSGeometryDistance(currentLocation!, CLLocationCoordinate2D(latitude: store.lattitude, longitude: store.longitude))

            if distance < minDistance {
                minDistance = distance
                nearestStore = store
            }

            //show nearest store
            mapView.animate(toLocation: CLLocationCoordinate2D(latitude: (nearestStore?.lattitude)!, longitude: (nearestStore?.longitude)!))
            mapView.animate(toZoom: 17)
        }
    }

    func getAdress(location: CLLocationCoordinate2D, completion: @escaping (_ address: CLPlacemark?, _ error: Error?) -> ()) {
        let geoCoder = CLGeocoder()
        let loc = CLLocation(latitude: location.latitude, longitude: location.longitude)
        geoCoder.reverseGeocodeLocation(loc) { placemarks, error in
            if let e = error {
                completion(nil, e)
            } else {
                let placeArray = placemarks
                var placeMark: CLPlacemark!
                placeMark = placeArray?[0]
                guard let address = placeMark else { return }
                completion(address, nil)
            }
        }
    }
    
}


extension StoreLocator: GMSMapViewDelegate{

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        infoView.isHidden = false
        infoView.title.text = marker.title
        for store in stores {
            print(store.name, marker.title!, store.address)
            if store.name == marker.title! {
                infoView.subtitleLabel.text = store.address
            }
        }
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        infoView.isHidden = true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoView.isHidden = true
    }
}

extension StoreLocator: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")

        currentLocation = locValue

        let location: CLLocation = locations.first!
        mapView.animate(toZoom: zoomLevel)
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        
        locationManager.stopUpdatingLocation()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted: print("Location access was restricted.")
        case .denied: print("User denied access to location.")
        case .notDetermined: print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse: print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

