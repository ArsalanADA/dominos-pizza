//
//  Menu.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 29.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

var indexSelected: Int = -1

let categories: [(mainImage: UIImage, PDFImage: UIImage, title: String, categoryCode: String)] = [(#imageLiteral(resourceName: "toyuqlar"), #imageLiteral(resourceName: "toyuq"), "Wings", "Wings"), (#imageLiteral(resourceName: "corekler"), #imageLiteral(resourceName: "corek"), "Bread", "Bread"), (#imageLiteral(resourceName: "salat"), #imageLiteral(resourceName: "salad"), "Salad", "Salad"), (#imageLiteral(resourceName: "drinks"), #imageLiteral(resourceName: "cola"), "Drink", "Drink"), (#imageLiteral(resourceName: "desert"), #imageLiteral(resourceName: "cake"), "Dessert", "Desert"), (#imageLiteral(resourceName: "sous"), #imageLiteral(resourceName: "ketchup"), "Souce", "Souce")]

class Menu: UITableViewController {
    
    @IBOutlet var titleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = titleView
        NotificationCenter.default.addObserver(self, selector: #selector(languageChanged), name: .languageChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addNumberOfOrdersBadge), name: .newOrderAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("test"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showProducts), name: NSNotification.Name("showProducts"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .delivaryMethodChanged, object: nil)
        
        addNumberOfOrdersBadge()
    }
    
    @objc func showProducts() {
        self.performSegue(withIdentifier: "products", sender: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addNumberOfOrdersBadge()
    }
    
    @objc func reload() {
        self.tableView.reloadData()
    }
    
    @objc func addNumberOfOrdersBadge() {
        // badge label
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: 20, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.textColor = .white
        label.backgroundColor = .red
        label.text = "\(orders.count)"
        
        if orders.count == 0 {
            label.isHidden = true
        } else {
            label.isHidden = false
        }
        
        label.adjustsFontSizeToFitWidth = true
        
        // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage(#imageLiteral(resourceName: "cartIcon"), for: .normal)
        rightButton.tintColor = .white
        rightButton.setTitleColor(.white, for: .normal)
        rightButton.addTarget(self, action: #selector(showBasket), for: .touchUpInside)
        rightButton.addSubview(label)
        
        // Bar button item
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButtomItem
    }
    
    @objc func showBasket() {
        performSegue(withIdentifier: "showBasket", sender: self)
    }
    
    @objc func languageChanged() {
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ProductsCollectionController {
            if indexSelected == 1 {         // pizza
                model = pizzas
                destination.isPizzaSelected = true
                destination.customTitle = "Pizzas".localized()
            } else if indexSelected > 2 {   // collection View
                destination.customTitle = categories[selectedCellIndex].title.localized()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delivaryMethod == .delivery ? 3 : 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var promotionMenuCell: PromotionMenuCell = PromotionMenuCell()
        var collectionCell: CollectionTableCell = CollectionTableCell()
        
        if delivaryMethod == .delivery {
            switch indexPath.row {
            case 0:
                promotionMenuCell = tableView.dequeueReusableCell(withIdentifier: "promotionCell", for: indexPath) as! PromotionMenuCell
                promotionMenuCell.backgroundImaheView.image = #imageLiteral(resourceName: "kampaniyalar")
                promotionMenuCell.productImageView.image = #imageLiteral(resourceName: "promotionIcon")
                promotionMenuCell.productLabel.text = "promotions".localized()
                promotionMenuCell.isPromotions = true
                return promotionMenuCell
                
            case 1:
                collectionCell = tableView.dequeueReusableCell(withIdentifier: "collectionCell", for: indexPath) as! CollectionTableCell
                return collectionCell
                
            case 2:
                promotionMenuCell = tableView.dequeueReusableCell(withIdentifier: "promotionCell", for: indexPath) as! PromotionMenuCell
                promotionMenuCell.backgroundImaheView.image = #imageLiteral(resourceName: "Pizzalar")
                promotionMenuCell.productImageView.image = #imageLiteral(resourceName: "pizza")
                promotionMenuCell.productLabel.text = "Pizzas".localized()
                selectedCategory = .pizza
                return promotionMenuCell
                
            default: break
            }
        } else {
            switch indexPath.row {
            case 0:
                collectionCell = tableView.dequeueReusableCell(withIdentifier: "collectionCell", for: indexPath) as! CollectionTableCell
                return collectionCell

            case 1:
                promotionMenuCell = tableView.dequeueReusableCell(withIdentifier: "promotionCell", for: indexPath) as! PromotionMenuCell
                promotionMenuCell.backgroundImaheView.image = #imageLiteral(resourceName: "kampaniyalar")
                promotionMenuCell.productImageView.image = #imageLiteral(resourceName: "promotionIcon")
                promotionMenuCell.productLabel.text = "pizzaAndPromotions".localized()
                promotionMenuCell.isPromotions = true
                return promotionMenuCell
                
            default: break
            }
        }
        return promotionMenuCell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if delivaryMethod == .delivery {
            if indexPath.row == 1 {
                return globalSizeForCollectionView
            }
        } else {
            if indexPath.row == 0 {
                return globalSizeForCollectionView
            }
        }
        return globalSizeForCollectionView / 3.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            indexSelected = 0
            performSegue(withIdentifier: "promotions", sender: self)
        } else if indexPath.row == 1 {
            indexSelected = 1
            performSegue(withIdentifier: "products", sender: self)
        } else {
            indexSelected = 2
            performSegue(withIdentifier: "products", sender: self)
        }
    }

}













