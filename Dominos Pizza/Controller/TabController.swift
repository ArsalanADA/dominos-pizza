//
//  TabController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 21.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Localize_Swift

let isFirstTime = "isFirstTime"

class TabController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: isFirstTime) != nil{
            UserDefaults.standard.setValue(false, forKey: isFirstTime)
        } else {
            UserDefaults.standard.setValue(true, forKey: isFirstTime)
            Localize.setCurrentLanguage("az")
            NotificationCenter.default.post(name: .languageChanged, object: nil)
        }


        if let arrayOfTabBarItems = self.tabBar.items {
            let tabBarItem = arrayOfTabBarItems[1]
            tabBarItem.isEnabled = false
        }

        self.tabBar.unselectedItemTintColor = .gray
        self.tabBar.tintColor = .white
        
        NotificationCenter.default.addObserver(self, selector: #selector(localize), name: .languageChanged, object: nil)
    }
    
    @objc func localize() {
        tabBar.items?[0].title = "Home".localized()
        tabBar.items?[1].title = "Menu".localized()
    }
    
}







