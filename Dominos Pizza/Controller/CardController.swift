//
//  CardController.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 01.12.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

var orders: [Order] = []

class CardController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.view.backgroundColor = .clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .orderDeleted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .pizzaAddDeleted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deliveryChanged), name: .delivaryMethodChanged, object: nil)
        
    }
    
    @objc func deliveryChanged() {
        orders.removeAll()
        tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        reload()
    }

    @objc func reload() {
        self.tableView.reloadData()
        if orders.isEmpty {
            var viewControllers = navigationController?.viewControllers
            viewControllers?.removeLast(1) //here 2 views to pop index numbers of views
            navigationController?.setViewControllers(viewControllers!, animated: true)
        }
    }
    
    @objc func added() {
        self.tableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        if !orders.isEmpty {
            self.tableView.isScrollEnabled = true
        }
        self.tableView.reloadData()
    }

}

extension CardController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orders.isEmpty {
            self.tableView.isScrollEnabled = false
            return 1
        }
        self.tableView.isScrollEnabled = true
        return orders.count + 3
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var orderCell = OrderSebetimCell()
        var orderPizzaCell = OrderPizzaSebetimCell()
        var toplamCell = ToplamSebetimCell()
        var emptyCartCell = EmptyCardCell()
        var detailCell = DetailCell()
        var suggestionCell = SuggestionCell()

        if orders.isEmpty {
            emptyCartCell = tableView.dequeueReusableCell(withIdentifier: "empty", for: indexPath) as! EmptyCardCell
            return emptyCartCell
        }

        else if indexPath.row == tableView.numberOfRows(inSection: 0) - 3 {
            toplamCell = tableView.dequeueReusableCell(withIdentifier: "total") as! ToplamSebetimCell
            return toplamCell
        }

        else if indexPath.row == tableView.numberOfRows(inSection: 0) - 2 {
            suggestionCell = tableView.dequeueReusableCell(withIdentifier: "suggestionCell") as! SuggestionCell
            return suggestionCell
        }

        else if indexPath.row == tableView.numberOfRows(inSection: 0) - 1 {
            detailCell = tableView.dequeueReusableCell(withIdentifier: "details") as! DetailCell
            return detailCell
        }

        else {
            if orders[indexPath.row].food is AbstractPizza {
                orderPizzaCell = tableView.dequeueReusableCell(withIdentifier: "productPizza", for: indexPath) as! OrderPizzaSebetimCell
                orderPizzaCell.orderObject = orders[indexPath.row]
                return orderPizzaCell
            } else {
                orderCell = tableView.dequeueReusableCell(withIdentifier: "product", for: indexPath) as! OrderSebetimCell
                orderCell.orderObject = orders[indexPath.row]
                return orderCell
            }
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if orders.isEmpty {
            return self.view.frame.height + 10
        } else {
            if indexPath.row == tableView.numberOfRows(inSection: 0) - 3 { // total
                return 80
            } else if indexPath.row == tableView.numberOfRows(inSection: 0) - 2 { // suggestions
                return 181
            } else if indexPath.row == tableView.numberOfRows(inSection: 0) - 1 { // details
                return 350
            } else if orders[indexPath.row].food is AbstractPizza {
                return pizzaAddsCellHeight
            }
        }
        return 150
    }

    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension CardController: CartWasCleanedDelegate {
    
    func cardWasCleaned() {
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
        self.tableView.isScrollEnabled = false
    }
    
}









