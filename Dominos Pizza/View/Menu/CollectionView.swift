//
//  CollectionView.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 29.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

class CollectionView: UICollectionView, UICollectionViewDelegate {
    
    var myCustom: Sizeable!
    
    let numberOfCellInARow: CGFloat = 2
    
    override func awakeFromNib() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .languageChanged, object: nil)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0

        let widthOfCell = (UIScreen.main.bounds.width - layout.sectionInset.left - layout.sectionInset.right -
            ((numberOfCellInARow - 1) * layout.minimumInteritemSpacing )) / numberOfCellInARow

        let cellSize = CGSize(width: widthOfCell, height: widthOfCell)
        layout.itemSize = cellSize

        myCustom.sizeOfCell(size: cellSize)

        setCollectionViewLayout(layout, animated: true)
    }
    
    @objc func reload() {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}
