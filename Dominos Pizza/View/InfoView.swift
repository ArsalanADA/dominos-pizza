//
//  InfoView.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 29.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class InfoView: UIView {

    @IBOutlet var title: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var button: UIButtonX!

    override func awakeFromNib() {
        subtitleLabel.adjustsFontSizeToFitWidth = true
        title.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func s(_ sender: Any) {
        
    }
    
}
