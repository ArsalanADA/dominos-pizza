//
//  GridLayout.swift
//  GridLayout
//
//  Created by Sztanyi Szabolcs on 2016. 11. 01..
//  Copyright © 2016. Sabminder. All rights reserved.
//

import UIKit

class GridLayout: UICollectionViewFlowLayout {
    
    var numberOfCellsInRow: CGFloat = 2
    
    init(numberOfColumns: Int) {
        super.init()
        
        minimumLineSpacing = 10 // vertical spacing
        minimumInteritemSpacing = 10 // horizontal spacing
        
        numberOfCellsInRow = CGFloat(numberOfColumns)
        headerReferenceSize = CGSize(width: 100, height: 50)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var itemSize: CGSize {
        get {
            if collectionView != nil {
                sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                
                let widthOfCell = (UIScreen.main.bounds.width - sectionInset.left - sectionInset.right -
                    ((numberOfCellsInRow - 1) * minimumInteritemSpacing) ) / numberOfCellsInRow
                return CGSize(width: widthOfCell, height: widthOfCell * 1.45)
            }

            // Default fallback
            return CGSize(width: 100, height: 100)
        }
        set {
            super.itemSize = newValue
        }
    }

    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return proposedContentOffset
    }

}
