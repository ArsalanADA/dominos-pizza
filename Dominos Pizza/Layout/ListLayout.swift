//
//  ListLayout.swift
//  GridLayout
//
//  Created by Sztanyi Szabolcs on 2016. 11. 26..
//  Copyright © 2016. Sabminder. All rights reserved.
//

import UIKit

class ListLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()
        
        minimumLineSpacing = 10
//        minimumInteritemSpacing = 10
        headerReferenceSize = CGSize(width: 100, height: 50)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var itemSize: CGSize {
        get {
            if collectionView != nil {
                sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                let widthOfCell = UIScreen.main.bounds.width - sectionInset.left - sectionInset.right
                return CGSize(width: widthOfCell, height: widthOfCell * 0.45)
            }

            // Default fallback
            return CGSize(width: 100, height: 100)
        }
        set {
            super.itemSize = newValue
        }
    }

    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return proposedContentOffset
    }
    
}
