
//
//  File.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 21.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let isEditing = Notification.Name("editing")
    static let stepperValueChanged = Notification.Name("stepper")
    static let newOrderAdded = Notification.Name("newOrderAdded")
    
    // Melumat
    static let stepperChanged = Notification.Name("stepperChanged")
    static let sizeChanged = NSNotification.Name("sizeChanged")
    
    
    static let promotionsDownloaded = NSNotification.Name("promotionsDownloaded")
    static let promotionProductsDownloaded = NSNotification.Name("promotionProductsDownloaded")

    
    static let newProductAdded = NSNotification.Name("newProductAdded")
    
    //Sebetim
    static let orderDeleted = Notification.Name("orderDeleted")
    //    static let showStepperInCard = Notification.Name("showStepperInCard")
    
    static let pizzaAddDeleted = NSNotification.Name("pizzaAddDeleted")
    static let checkoutPressed = NSNotification.Name("checkoutPressed")
    
    static let apiDownloaded = Notification.Name("apiDownloaded")
    static let detailsDownloaded = Notification.Name("detailsDownloaded")
    static let reloadProducts = Notification.Name("reloadProducts")
    
        static let foundProduct = Notification.Name("foundProduct")

    static let starPressed = Notification.Name("starPressed")

        static let orderChangePressed = Notification.Name("orderChangePressed")

    static let reload22 = Notification.Name("reload22")

    static let delivaryMethodChanged = Notification.Name("delivaryMethodChanged")

    static let toggleMenu = NSNotification.Name("toggleMenu")


    static let phoneEditPressed = Notification.Name("phoneEditPressed")
    static let addressEditPressed = Notification.Name("addressEditPressed")
    
    static let registerPressed = NSNotification.Name("registerPressed")
    static let loginPressed = NSNotification.Name("loginPressed")
    
    static let membershipInfoChanged = NSNotification.Name("membershipInfoChanged")
    
    static let languageChanged = NSNotification.Name("languageChanged")
    
    static let addressDownloaded = NSNotification.Name("addressDownloaded")
    static let myAddressDownloaded = NSNotification.Name("myAddressDownloaded")

    static let addressDeleted = NSNotification.Name("addressDeleted")

    static let suggestionPressed = NSNotification.Name("suggestionPressed")

    static let doughTypeChanged = NSNotification.Name("doughTypeChanged")
    static let rimChanged = NSNotification.Name("rimChanged")
    static let ingredientChanged = NSNotification.Name("ingredientChanged")
    static let extraIngredientChanged = NSNotification.Name("extraIngredientChanged")

    static let orderSubmitted = NSNotification.Name("orderSubmitted")
    
    static let addressSelected = NSNotification.Name("addressSelected")
    
    static let userChanged = NSNotification.Name("userChanged")

    static let membershipInfoPressed = NSNotification.Name("membershipInfoPressed")
    static let myAddressesPressed = NSNotification.Name("myAddressesPressed")
    static let favoritesPressed = NSNotification.Name("favoritesPressed")
    static let ordersPressed = NSNotification.Name("ordersPressed")

    static let orderHistoryDownloaded = NSNotification.Name("orderHistoryDownloaded")
    
    static let storesDownloaded = NSNotification.Name("storesDownloaded")    
    
    static let showMembership = NSNotification.Name("showMembership")
    static let showOrders = NSNotification.Name("showOrders")
    static let showMyFavorite = NSNotification.Name("showMyFavorite")
    static let showLogin = NSNotification.Name("showLogin")
    
}











