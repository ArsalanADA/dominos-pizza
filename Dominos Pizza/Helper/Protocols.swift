//
//  Protocols.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 21.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation
import UIKit

protocol CartWasCleanedDelegate: class {
    func cardWasCleaned()
}

protocol Sizeable {
    func sizeOfCell(size: CGSize)
}

extension UIImageView {
    func shake(duration: CFTimeInterval) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration
        animation.repeatCount = 1
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(animation, forKey: "position")
        
    }
}






