//
//  Error.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 23.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation

enum FontError: Error {
    case invalidFont
}
