//
//  DownloadAllProducts.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 24.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Localize_Swift

var chickens: [MODEL] = []
var breads: [MODEL] = []
var salads: [MODEL] = []
var drinks: [MODEL] = []
var desserts: [MODEL] = []
var sauces: [MODEL] = []
var pizzas: [MODEL] = []

var request: DataRequest?
var request2: DataRequest?

func downloadAllProducts() {
    downloadPizzas()
    download()
}

func downloadListOfFavorites() {
    let favoriteURL = baseURL + "user/favorites"

    guard let user = currentUser() else {return}
    let parameters: Parameters = [:]

    print(user.token)

    let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]

    Alamofire.request(favoriteURL, method: .get, parameters: parameters, encoding: URLEncoding.httpBody, headers: header).validate().responseJSON { (response) in

        switch response.result {
        case .success:
            var arrayOfFavoriteProductCodes: [String] = []
            if let value = response.result.value {
                let json = JSON(value)
                let favorites = json["data"]["favorites"]
                for (_, subJson) in favorites {
                    let productCode = subJson["ProductCode"].stringValue
                    arrayOfFavoriteProductCodes.append(productCode)
                }

                // Update pizzas
                for c in 0 ..< arrayOfFavoriteProductCodes.count {
                    for p in 0 ..< pizzas.count {
                        if pizzas[p].food.productCode == arrayOfFavoriteProductCodes[c] {
                            pizzas[p].food.isFavorite = true
                        }
                    }
                }

                NotificationCenter.default.post(name: .reloadProducts, object: nil)
            }
        case .failure(let error):
            print(error.localizedDescription)
        }
    }
}

func downloadPizzas() {
    let stringURL = baseURL + "category/Pizza?language=\(Localize.currentLanguage())"

    pizzas.removeAll()

    request = Alamofire.request(stringURL).validate().responseJSON { (response) in
        switch response.result {
        case .success:
            if let value = response.result.value {

                let makeYourOwnPizza = Sous(name: "Make Your Own Pizza", image: #imageLiteral(resourceName: "makePizzaImage"), productCode: "PIZZA", isFavorite: false, surcharge: -10.0, surchargelessPrice: -10.0, productSizeCode: nil)
                pizzas.append(MODEL(food: makeYourOwnPizza, ingredients: [], isSpecial: true))

                let json = JSON(value)
                let products = json["data"]["products"]

                for (_, subJson) in products {
                    guard let code = subJson["ProductCode"].string else { return }

                    let name = subJson["ProductDescription"].string ?? "-1"
                    let ingrediensJSON = subJson["ingredients"]
                    let surcharge = subJson["SurchargelessPrice"].doubleValue
                    let surchargless = subJson["Surcharge"].doubleValue
                    let isSpecial = subJson["isSpecial"].string ?? "" == "1"

                    //ingredients
                    var arrayOfIngredients: [Ingredient] = []

                    guard name.count != 0 else  {
                        continue
                    }

                    for (_ , subJson) in ingrediensJSON {
                        let description = subJson["Description"].string ?? "12"
                        let productOptionCode = subJson["ProductOptionCode"].string ?? "12"
                        let ingredient = Ingredient(name: description, price: 0.0, isSelected: true, productOptionCode: productOptionCode)
                        arrayOfIngredients.append(ingredient)
                    }

                    guard let url: URL = URL(string: "http://www.dominospizza.az/rest/medium/ProductOsg/Detail/\(code)/Resize/224x224/AZ/view") else {return}

                    // FIXME: SOUS as a PIZZA
                    pizzas.append(MODEL(food: Sous(name: name, image: UIImage(), productCode: code, surcharge: surcharge, surchargelessPrice: surchargless, productSizeCode: nil), ingredients: arrayOfIngredients, isSpecial: isSpecial))

                    pizzas.sort(by: { (m1, m2) -> Bool in
                        m1.isSpecial && !m2.isSpecial
                    })

                    getData(from: url) { data, response, error in
                        guard let data = data, error == nil, let image = UIImage(data: data) else {return}
                        for i in 0 ..< pizzas.count {
                            if url.absoluteString == "http://www.dominospizza.az/rest/medium/ProductOsg/Detail/\(pizzas[i].food.productCode)/Resize/224x224/AZ/view" {
                                pizzas[i].food.image = image
                                NotificationCenter.default.post(name: .reloadProducts, object: nil)
                            }
                        }
                    }
                }
            }
        case .failure(let error):
            print(error.localizedDescription)
        }

        downloadListOfFavorites()

        NotificationCenter.default.post(name: .reloadProducts, object: nil)
    }
}

func download() {
    request2?.cancel()

    salads.removeAll()
    drinks.removeAll()
    chickens.removeAll()
    breads.removeAll()
    desserts.removeAll()
    sauces.removeAll()

    for code in categories {
        let categoryCode = code.categoryCode
        let stringURL = baseURL + "category/\(categoryCode)?language=\(Localize.currentLanguage())"

        request2 = Alamofire.request(stringURL).validate().responseJSON { (response) in

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let products = json["data"]["products"]

                    for (_, subJson) in products {
                        guard let code = subJson["ProductCode"].string else { return }

                        if code == "_500MLHEIN" {
                            continue
                        }

                        let name = subJson["ProductDescription"].string ?? "k"
                        let ingrediensJSON = subJson["ingredients"]
                        let surcharge = subJson["Surcharge"].doubleValue
                        let surchargless = subJson["SurchargelessPrice"].doubleValue
                        let isSpecial = subJson["isSpecial"].string ?? "" == "1"

                        //ingredients
                        var arrayOfIngredients: [Ingredient] = []

                        for (_ , subJson) in ingrediensJSON {
                            let description = subJson["Description"].string ?? ""
                            let productOptionCode = subJson["ProductOptionCode"].string ?? ""
                            let ingredient = Ingredient(name: description, price: 0.0, isSelected: true, productOptionCode: productOptionCode)
                            arrayOfIngredients.append(ingredient)
                        }

                        guard let url: URL = URL(string: "http://www.dominospizza.az/rest/medium/Product/Detail/\(code)/Resize/224x224/AZ/View") else {return}

                        getData(from: url) { data, response, error in
                            guard let data = data, error == nil else { return }

                            DispatchQueue.main.async {
                                guard let image = UIImage(data: data) else {return}

                                switch categoryCode {
                                case "Salad": salads.append(MODEL(food: Salad(name: name, image: image, productCode: code, surcharge: surcharge, surchargelessPrice: surchargless, productSizeCode: nil), ingredients: [], isSpecial: isSpecial))

                                case "Drink": drinks.append(MODEL(food: Drink(name: name, image: image, productCode: code, surcharge: surcharge, surchargelessPrice: surchargless, productSizeCode: nil), ingredients: [], isSpecial: isSpecial))

                                case "Wings": chickens.append(MODEL(food: Chicken(name: name, image:
                                    image, productCode: code, surcharge: surcharge, surchargelessPrice: surchargless, productSizeCode: nil), ingredients: [], isSpecial: isSpecial))

                                case "Bread": breads.append(MODEL(food: Bread(name: name, image: image, productCode: code, surcharge: surcharge, surchargelessPrice: surchargless, productSizeCode: nil), ingredients: [], isSpecial: isSpecial))

                                case "Desert": desserts.append(MODEL(food: Dessert(name: name, image: image, productCode: code, surcharge: surcharge, surchargelessPrice: surchargless, productSizeCode: nil), ingredients: [], isSpecial: isSpecial))

                                case "Souce": sauces.append(MODEL(food: Sous(name: name, image: image, productCode: code, surcharge: surcharge, surchargelessPrice: surchargless, productSizeCode: nil), ingredients: [], isSpecial: isSpecial))

                                default: break
                                }

                                NotificationCenter.default.post(name: .reloadProducts, object: nil)
                            }
                        }
                    }
                }
            case .failure(let error):
                print(error)
            }

            NotificationCenter.default.post(name: .reloadProducts, object: nil)
        }
    }
}








