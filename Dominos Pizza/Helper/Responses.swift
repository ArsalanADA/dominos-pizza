//
//  Responses.swift
//  Dominos Pizza
//
//  Created by Фархад on 10.06.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import Foundation

struct PromotionResponse: Codable {
    var success: Bool?
    var data: [Promotion]?
}

//struct PromotionResponse: Codable {
//    var success: Bool?
//    var data: [Promotion]?
//}



struct PromotionProductResponse: Codable {
    var success: Bool?
    var data: PromotionProductDataResponse?
}

struct PromotionProductDataResponse: Codable {
    var count: Double?
    var products: [PromotionProduct]?
}


