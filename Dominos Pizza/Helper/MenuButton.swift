//
//  MenuButton.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 27.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation
import UIKit

struct MenuButton {
    let title: String
    let subtitle: String
    let image: UIImage
}
