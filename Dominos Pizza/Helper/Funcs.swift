//
//  Funcs
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 23.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Localize_Swift

/// Find index of order in orders
///
/// - Parameter order: order itself
/// - Returns: index of order
func findIndex(of order: Order?) -> Int? {
    for i in 0 ..< orders.count {
        if order == orders[i] {
            return i
        }
    }
    return nil
}

func currentUser() -> User? {
    guard
        let data = UserDefaults.standard.data(forKey: userKeyString),
        let user: User = NSKeyedUnarchiver.unarchiveObject(with: data) as? User else {return nil}
    
    print("Token:", user.token)
    return user
}

func addToOrderList(food: Food, quantity: Int) {
    if orders.contains(Order(food: food, quantity: quantity)) && !(food is AbstractPizza) {
        if let index = orders.index(of: Order(food: food, quantity: quantity)) {
            orders[index].quantity += quantity
        }
    }
    else if orders.contains(Order(food: food, quantity: quantity)) && food is AbstractPizza {
        if let index = orders.index(of: Order(food: food, quantity: quantity)) {
            orders[index].quantity += quantity
        }
    }
    else {
        orders.append(Order(food: food, quantity: quantity))
    }
}

func calculatePrice(of food: Food?) -> Double? {
    var totalPrice = 0.0
    
    if food is AbstractPizza {
        guard let pizza = food as? AbstractPizza else {return nil}
        let selectedExtraIngredients = pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients()
        
        print(pizza.selectedSize)
        
        var sizeCode = ""
        switch pizza.selectedSize {
        case .small: sizeCode = "8"
        case .medium: sizeCode = "10"
        case .big: sizeCode = "12"
        }
        
        print("COUNT IS :", pizza.products.count)
        
        for product in pizza.products {
            if product.sizeCode == sizeCode {
                if product.doughCode == pizza.pizzas[pizza.selectedSize.rawValue].selectedDough()?.code {
                    if product.sideCode == pizza.pizzas[pizza.selectedSize.rawValue].selectedRim()?.sideCode {
                        totalPrice = product.price
                        print("TOTALPRICE IS", product.price)
                    }
                }
            }
        }
        
        for extraIngredient in selectedExtraIngredients {
            totalPrice += extraIngredient.price
        }
        
    } else {
        guard food != nil else {return nil}
        totalPrice += food!.surchargelessPrice
    }
    
    return totalPrice
}

func flatArrayOfStrings(_ string: [String]) -> String {
    return string.joined(separator: ", ")
}

func flatArrayOfIngredients(_ string: [Ingredient]) -> String {
    var test: [String] = []
    for ingredient in string {
        test.append(ingredient.name)
    }
    return test.joined(separator: ", ")
}

func prefix(user: User) -> String {
    if user.sex == .male {
        return "Mr."
    } else {
        return "Mrs."
    }
}

func returnSize(_ size: String) -> Size {
    
    switch size {
    case "8": return .small
    case "10": return .medium
    case "12": return .big
    default: break
    }
    
    print("EROR WITH SIZE. WRong code:", size)
    return .small
}

func returnPromotionSize(_ size: String) -> PromotionSize {
    
    switch size {
    case "8": return .small
    case "10": return .medium
    case "12": return .big
    case "2LTB": return .litr
    default: break
    }
    
    print("EROR WITH SIZE. WRong code:", size)
    return .small
}


/// Validate email string
///
/// - Parameter email: A String that represent an email address
/// - Returns: A Boolean value indicating whether an email is valid.
func isValid(_ email: String) -> Bool {
    let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    
    let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
    return emailTest.evaluate(with: email)
}

func downloadPromotions() {
    let url = baseURL + "campaigns?language=\(Localize.currentLanguage())"
    Alamofire.request(url).validate().responseString { (response) in
        do {
            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                guard let data = response.data else {return}
                let response = try decoder.decode(PromotionResponse.self, from: data)
                promotions = response.data!
                NotificationCenter.default.post(name: .promotionsDownloaded, object: nil)
            case .failure: break
            }
        } catch {}
    }
}


func downloadAddresses() {
    let url = baseURL + "user/address"
    
    addresses.removeAll()
    
    guard let user = currentUser() else {return}
    
    let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)" ]
    
    Alamofire.request(url, method: .get, parameters: [:], encoding: URLEncoding.httpBody, headers: header).responseJSON { (response) in
        switch response.result {
        case .success:
            if let value = response.result.value {
                let json = JSON(value)
                print(json)
                
                let data = json["data"]["countries"]
                
                for (_ , subJson) in data {
                    let countryCode = subJson["CountyCode"].string
                    let cityCode = subJson["CityCode"].string
                    let disctrictCode = subJson["DistrictCode"].string
                    let regionName = subJson["Name"].string
                    let isActive = subJson["IsActive"].string
                    
                    let streetsJSON = subJson["streets"]
                    
                    var streets: [Street] = []
                    
                    for (_, subJSON2) in streetsJSON {
                        let streetCode = subJSON2["StreetCode"].string
                        let cityCode = subJSON2["CityCode"].string
                        let countryCode = subJSON2["CountyCode"].string
                        let streetName = subJSON2["Name"].string
                        
                        let street = Street(streetCode: streetCode, cityCode: cityCode, countryCode: countryCode, name: streetName)
                        streets.append(street)
                    }
                    
                    var isActi = false
                    if isActive == "1" {
                        isActi = true
                    }
                    
                    let address = Address(cityCode: cityCode, countyCode: countryCode, districtCode: disctrictCode, regionName: regionName, isActive: isActi, streets: streets)
                    
                    addresses.append(address)
                    NotificationCenter.default.post(name: .addressDownloaded, object: nil)
                }
                
            }
        case .failure(let error):
            print(error)
        }
    }
    
}

func downloadHistoryOrders() {
    let url = baseURL + "order/my-orders"
    
    myOrders.removeAll()
    
    guard let user = currentUser() else {return}
    
    let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]
    
    Alamofire.request(url, method: .get, parameters: [:], encoding: URLEncoding.httpBody, headers: header).responseJSON { (response) in
        switch response.result {
        case .success:
            if let value = response.result.value {
                let json = JSON(value)
                
                let orders = json["data"]["orders"]
                print(orders)
                
                for (_ , subJson) in orders {
                    let id = subJson["OrderID"].stringValue
                    let text = subJson["OrderText"].stringValue
                    let status = subJson["Status"].stringValue
                    let store = subJson["Store"].stringValue
                    let paymentPrice = subJson["PaymentPrice"].doubleValue
                    let date = subJson["CreatedOn"].stringValue
                    
                    let order: HistoryOrder = HistoryOrder(id: id, text: text, status: status, store: store, price: paymentPrice, date: date)
                    
                    myOrders.append(order)
                    
                    NotificationCenter.default.post(name: .orderHistoryDownloaded, object: nil)
                }
            }
        case .failure(let error):
            print(error)
        }
    }
}

func downloadMyAddresses() {
    let url = baseURL + "user/my-addresses"
    
    myAddresses.removeAll()
    
    guard let user = currentUser() else {return}
    
    let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]

    Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response) in

        switch response.result {
        case .success:
            if let value = response.result.value {
                let json = JSON(value)
                
                let addresses = json["data"]["addresses"]
                
                print(addresses)
                
                for (_ , subJson) in addresses {
                    let apartment = subJson["Apartment"].string
                    let cityCode = subJson["CityCode"].string
                    let countyCode = subJson["CountyCode"].string
                    let doorNumber = subJson["DoorNumber"].string
                    let customerID = subJson["CustomerID"].string
                    let customerAddressID = subJson["CustomerAddressID"].string
                    let districtCode = subJson["DistrictCode"].string
                    let instructions = subJson["Instructions"].string
                    let phone = subJson["Phone"].string
                    //                    let roadName = subJson["RoadName"].string
                    let streetName = subJson["StreetName"].string
                    let regionName = subJson["county"]["Name"].string
                    let locationCode = subJson["county"]["location"]["StoreLocationCode"].string
                    
                    // FIXME: Location Code
                    let address = MyAddress(isFavorite: false, apartment: apartment, cityCode: cityCode, countryCode: countyCode, customerID: customerID, customerAddressID: customerAddressID, dictrictCode: districtCode, doorNumber: doorNumber, instructions: instructions, phone: phone, streetName: streetName, regionName: regionName, locationCode: locationCode)
                    
                    myAddresses.append(address)
                    
                    NotificationCenter.default.post(name: .myAddressDownloaded, object: nil)
                }
                
                // If there is only one address it will be favorite
                if myAddresses.count == 1 {
                    myAddresses[0].isFavorite = true
                    UserDefaults.standard.set(myAddresses[0].customerAddressID, forKey: "custometAddressID")
                } else {
                    // Load from userdefaults
                    if let custometAddressID: String = UserDefaults.standard.value(forKey: "custometAddressID") as? String {
                        for i in 0 ..< myAddresses.count {
                            if myAddresses[i].customerAddressID == custometAddressID {
                                myAddresses[i].isFavorite = true
                            }
                        }
                    } else {
                        if !myAddresses.isEmpty {
                            myAddresses[myAddresses.count - 1].isFavorite = true
                        }
                    }
                }
            }
        case .failure(let error):
            print(error)
        }
    }
}

func parse2PizzaO(JSONData json: JSON) -> AbstractPizza? {
    
    let ingredientJSON = json["data"]["productIngredients"]
    let extraIngredientJSON = json["data"]["ingredients"]
    let sidesJSON = json["data"]["sides"]
    let doughsJSON = json["data"]["doughs"]
    let productsJSON = json["data"]["products"]
    let sizesJSON = json["data"]["sizes"]
    
    var ingredients: [Ingredient] = []
    
    var smallDoughts: [Dough] = []
    var mediumDoughts: [Dough] = []
    var bigDoughts: [Dough] = []
    
    var smallSides: [Rim] = []
    var mediumSides: [Rim] = []
    var bigSides: [Rim] = []
    
    var smallExtraIngredients: [Ingredient] = []
    var mediumExtraIngredients: [Ingredient] = []
    var bigExtraIngredients: [Ingredient] = []
    
    var products: [Product] = []
    
    var smallPizza: KindOfPizza?
    var mediumPizza: KindOfPizza?
    var bigPizza: KindOfPizza?
    
    // doughs
    for (_, subJson) in doughsJSON {
        guard let description = subJson["Description"].string,
            let doughtCode = subJson["ProductDoughCode"].string,
            let sizeCode = subJson["ProductSizeCode"].string,
            let price = subJson["SurchargeAmount"].string
            else { return nil }
        
        var image: UIImage = #imageLiteral(resourceName: "classic-dough")
        switch doughtCode {
        case "HANDTOSS": image = #imageLiteral(resourceName: "classic-dough")
        case "THIN": image = #imageLiteral(resourceName: "thin-dough")
        case "_TKTBN": image = #imageLiteral(resourceName: "thinnest-dough")
        case "DEEPDISH": image = #imageLiteral(resourceName: "doublex")
        case "THDM": image = #imageLiteral(resourceName: "doublex")
        default: break
        }
        
        var dough = Dough(name: description, code: doughtCode, image: image, price: Double(price)!, isSelected: false)
        
        if dough.code == "HANDTOSS" {
            dough.isSelected = true
        }
        
        switch returnSize(sizeCode) as Size {
        case .small: smallDoughts.append(dough)
        case .medium: mediumDoughts.append(dough)
        case .big: bigDoughts.append(dough)
        }
    }
    
    
    // Sides
    for (_, subJson) in sidesJSON {
        guard let description = subJson["Description"].string,
            let productSideCode = subJson["ProductSideCode"].string,
            let sizeCode = subJson["ProductSizeCode"].string,
            let price = subJson["SurchargeAmount"].string
            else { return nil }
        
        var image = #imageLiteral(resourceName: "classic_side")
        switch productSideCode {
        case "NONE": image = #imageLiteral(resourceName: "classic_side")
        case "_STFCL": image = #imageLiteral(resourceName: "cheese_side")
        case "_PKCL": image = #imageLiteral(resourceName: "parmesan_side")
        case "_SKCL": image = #imageLiteral(resourceName: "garlic_side")
        case "_SSCK": image = #imageLiteral(resourceName: "sausage_side")
        default: break
        }
        
        var rim = Rim(name: description, sideCode: productSideCode, price: Double(price)!, image: image, isSelected: false)
        
        if rim.sideCode == "NONE" {
            rim.isSelected = true
        }
        
        switch returnSize(sizeCode) as Size {
        case .small: smallSides.append(rim)
        case .medium: mediumSides.append(rim)
        case .big: bigSides.append(rim)
        }
    }
    
    for (_, subJson) in ingredientJSON {
        guard let description = subJson["Description"].string,
            let productOptionCode = subJson["ProductOptionCode"].string
            else { return nil }
        ingredients.append(Ingredient(name: description, price: 0.0, isSelected: true, productOptionCode: productOptionCode))
    }
    
    var prices: [Double] = []
    
    for (_, subJson) in sizesJSON {
        let price = subJson["BaseOptionPrice"].doubleValue
        prices.append(price)
    }
    
    // Extra ingredients
    for (_, subJson) in extraIngredientJSON {
        guard let description = subJson["Description"].string,
            let productOptionCode = subJson["ProductOptionCode"].string
            else { return nil}
        
        let smallIngredient = Ingredient(name: description, price: prices[0], isSelected: false, productOptionCode: productOptionCode)
        let mediumIngredient = Ingredient(name: description, price: prices[1], isSelected: false, productOptionCode: productOptionCode)
        let bigIngredient = Ingredient(name: description, price: prices[2], isSelected: false, productOptionCode: productOptionCode)
        
        smallExtraIngredients.append(smallIngredient)
        mediumExtraIngredients.append(mediumIngredient)
        bigExtraIngredients.append(bigIngredient)
        
    }
    
    // Products
    for (_, s) in productsJSON {
        let productCode = s["ProductCode"].string
        let description = s["Desc"].string
        let categoryCode = s["CategoryCode"].string
        let pulseCatCode = s["PulseCatCode"].string
        let flavorCode = s["FlavorCode"].string
        let flavorName = s["FlavorName"].string
        let osgCode = s["OsgCode"].string
        let osgName = s["OsgName"].string
        let doughCode = s["DoughCode"].string
        let doughName = s["DoughName"].string
        let sideCode = s["SideCode"].string
        let sideName = s["SideName"].string
        let sizeCode = s["SizeCode"].string
        let sizeName = s["SizeName"].string
        let surchargelessPrice = s["SurchargelessPrice"].doubleValue
        let surcharge = s["surcharge"].doubleValue
        let price = s["Price"].doubleValue
        
        let product = Product(productCode: productCode!, description: description ?? "ARSALAN", categoryCode: categoryCode ?? "ARSALAN", pulseCatCode: pulseCatCode ?? "ARSALAN", flavorCode: flavorCode ?? "ARSALAN", flavorName: flavorName ?? "ARSALAN", osgCode: osgCode ?? "ARSALAN", osgName: osgName ?? "ARSALAN", doughCode: doughCode ?? "ARSALAN", doughName: doughName ?? "ARSALAN", sideCode: sideCode ?? "ARSALAN", sideName: sideName ?? "ARSALAN", sizeCode: sizeCode ?? "ARSALAN", sizeName: sizeName ?? "ARSALAN", price: price, size: returnSize(sizeCode!), surchargelessPrice: surchargelessPrice, surcharge: surcharge)
        
        products.append(product)
    }
    
    smallPizza = KindOfPizza(dough: smallDoughts, rim: smallSides, extraIngredients: smallExtraIngredients)
    mediumPizza = KindOfPizza(dough: mediumDoughts, rim: mediumSides, extraIngredients: mediumExtraIngredients)
    bigPizza = KindOfPizza(dough: bigDoughts, rim: bigSides, extraIngredients: bigExtraIngredients)
    
    guard let o = foodObject else {return nil}
    
    foodObject = AbstractPizza(name: o.name, code: o.productCode, pizzas: [smallPizza!, mediumPizza!, bigPizza!], image: o.image, ingredients: ingredients, products: products, selectedSize: .medium, productSizeCode: o.productSizeCode)
    
    NotificationCenter.default.post(name: .detailsDownloaded, object: nil)
    
    return foodObject as? AbstractPizza
}



func findSelectedProduct(in pizza: AbstractPizza) -> Product? {
    
    var sizeCode = ""
    
    switch pizza.selectedSize {
    case .small: sizeCode = "8"
    case .medium: sizeCode = "10"
    case .big: sizeCode = "12"
    }
    
    for product in pizza.products {
        if product.doughCode == pizza.pizzas[pizza.selectedSize.rawValue].selectedDough()?.code {
            if product.sideCode == pizza.pizzas[pizza.selectedSize.rawValue].selectedRim()?.sideCode {
                if product.sizeCode == sizeCode {
                    NotificationCenter.default.post(name: .foundProduct, object: nil)
                    return product
                }
            }
        }
    }
    return nil
}

/// Creates Disctionary from orders
///
/// - Parameter instruction: instructions from user regarding the order
/// - Returns: dictionary
func generateOrderDictionary(with instruction: String? = "") -> Parameters {
    var dictionary = Parameters()
    var orderDictionary = [String: Any]()
    var orderLinesDictionary = [Dictionary<String, Any>]()
    
    // FIXME: WRONG HARD LOCATION CODE
    orderDictionary["LocationCode"] = "97407"
    orderDictionary["CustomerAddressId"] = ""
    
    if let data = UserDefaults.standard.data(forKey: userKeyString),
        let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User {
        if !myAddresses.isEmpty {
            orderDictionary["LocationCode"] = user.selectedAddress()?.locationCode
        }
        orderDictionary["CustomerAddressId"] = user.selectedAddress()?.customerAddressID
    }
    
    orderDictionary["ChannelCode"] = "IPHONE"
    orderDictionary["LanguageCode"] = "\(Localize.currentLanguage().uppercased())"
    orderDictionary["ServiceTypeCode"] = delivaryMethod.rawValue
    orderDictionary["PaymentMethodCode"] = paymetMethod.rawValue
    orderDictionary["Instructions"] = instruction
    
    for order in orders {
        var orderLineDictionary = Dictionary<String, Any>()
        var arrayOfIngredients: [Dictionary<String, Any>] = []
        
        orderLineDictionary["ProductCode"] = order.food.productCode
        orderLineDictionary["Quantity"] = order.quantity
        orderLineDictionary["Surcharge"] = order.food.surcharge
        orderLineDictionary["SurchargelessPrice"] = order.food.surchargelessPrice
        
        // If order is pizza
        if let pizza = order.food as? AbstractPizza {
            
            // FIND AND ASSIGN PRODUCT FOR A PIZZA
            orderLineDictionary["ProductCode"] = findSelectedProduct(in: pizza)?.productCode ?? "no code"
            orderLineDictionary["categoryCode"] = "Pizza"
            orderLineDictionary["productDescription"] = findSelectedProduct(in: pizza)?.description
            orderLineDictionary["productDoughCode"] = findSelectedProduct(in: pizza)?.doughCode
            orderLineDictionary["productOsgCode"] = findSelectedProduct(in: pizza)?.osgCode
            orderLineDictionary["productOsgName"] = findSelectedProduct(in: pizza)?.osgName
            orderLineDictionary["productSideCode"] = findSelectedProduct(in: pizza)?.sideCode
            orderLineDictionary["productSizeCode"] = findSelectedProduct(in: pizza)?.sizeCode
            
            orderLineDictionary["Surcharge"] = Double((findSelectedProduct(in: pizza)?.surcharge) ?? -1)
            orderLineDictionary["SurchargelessPrice"] = Double((findSelectedProduct(in: pizza)?.surchargelessPrice) ?? -1)
            
            // Add toppings
            for topping in pizza.selectedToppings() {
                var quantity = 0
                if topping.isSelected {
                    quantity = 1
                }
                arrayOfIngredients.append(["OptionCode": topping.productOptionCode, "Quantity": quantity, "Price": topping.price])
            }
            
            // Add extra ingredients
            for extraIngredient in pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients() {
                var quantity = 0
                if extraIngredient.isSelected {
                    quantity = 1
                }
                arrayOfIngredients.append(["OptionCode": extraIngredient.productOptionCode, "Quantity": quantity, "Price": extraIngredient.price])
            }
            
            orderLineDictionary["Ingredients"] = arrayOfIngredients
        }
        orderLinesDictionary.append(orderLineDictionary)
    }
    
    dictionary["Order"] = orderDictionary
    dictionary["OrderLines"] = orderLinesDictionary
    
    return dictionary
}

func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
    URLSession.shared.dataTask(with: url) { data, response, error in
        completion(data, response, error)}.resume()
}













