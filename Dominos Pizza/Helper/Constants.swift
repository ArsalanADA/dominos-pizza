//
//  Constants.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 19.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Foundation

let userKeyString = "user"

let numberCodes = ["+99450", "+99451", "+99455", "+99470"]


var delivaryMethod: DeliveryMethod = .delivery
var paymetMethod: PaymentMethod = .cash

// new

let baseURL = "http://34.254.48.78/"

let trackingCodeKey = "trakingCode"

var selectedCategory: Category = .pizza

enum Category {
    case pizza, chicken, bread, salad, drink, dessert, sauce
}

var doughCC: CGFloat = 100
var rimCC: CGFloat = 100
var stToppings: CGFloat = 100
var exToppings: CGFloat = 100

var pizzaAddsCellHeight: CGFloat = 100

let redColor: UIColor = #colorLiteral(red: 0.890196085, green: 0.1411764771, blue: 0.2549019754, alpha: 1)
let blueColor: UIColor = #colorLiteral(red: 0.1019607857, green: 0.4627451003, blue: 0.631372571, alpha: 1)

var arrayOfFavorites = Set<Food>()
