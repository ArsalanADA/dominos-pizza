//
//  Converter.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 13.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import Foundation
import UIKit

func convertToPrice(price: Double, fontSize: CGFloat? = 18, color: UIColor? = redColor, isPlus: Bool? = false) -> NSAttributedString {
    
    if UIFont(name: "JISAZNRegular", size: fontSize!) != nil {
        //        print("ye1s")
    }
    
    if UIFont(name: "JIS AZN", size: fontSize!) != nil {
        //        print("yes2")
    }
    
    let combination = NSMutableAttributedString()
    
    if let f = UIFont(name: "JIS AZN", size: fontSize!), let ovsankaFont = UIFont(name: "Ovsyanka", size: fontSize!) {
        
        let price =  String(format: "%.2f ", price)
        
        let yourAttributes = [NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.font: ovsankaFont]

        let yourOtherAttributes = [NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.font: f]
        
        var partOne = NSMutableAttributedString(string: price, attributes: yourAttributes)
        
        if isPlus! {
            partOne = NSMutableAttributedString(string: "+" + price, attributes: yourAttributes)
        }
        
        let partTwo = NSMutableAttributedString(string: "M", attributes: yourOtherAttributes)
        
        combination.append(partOne)
        combination.append(partTwo)
        
    }
    return combination
}








