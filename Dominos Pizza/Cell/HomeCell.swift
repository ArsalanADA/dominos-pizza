//
//  MenuCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 27.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

// delete
class HomeCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        
        titleLabel.adjustsFontSizeToFitWidth = true
        subtitleLabel.adjustsFontSizeToFitWidth = true
        
        clipsToBounds = true
        layer.cornerRadius = layer.frame.height / 2.0
        
    }
}
