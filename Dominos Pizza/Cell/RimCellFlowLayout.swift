//
//  RimCellFlowLayout.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 04.05.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class RimCellFlowLayout: UICollectionViewFlowLayout {

    var numberOfCellsInRow: Int = 3

    init(numberOfColumns: Int) {
        super.init()

        minimumLineSpacing = 10
        minimumInteritemSpacing = 10
        numberOfCellsInRow = numberOfColumns
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var itemSize: CGSize {
        get {
            if collectionView != nil {
                sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
                
                let widthOfCell = (UIScreen.main.bounds.width - sectionInset.left - sectionInset.right - (CGFloat(numberOfCellsInRow - 1) * minimumInteritemSpacing)) / CGFloat(numberOfCellsInRow)
                return CGSize(width: widthOfCell, height: widthOfCell * 1.45)
            }

            // Default fallback
            return CGSize(width: 100, height: 100)
        }
        set {
            super.itemSize = newValue
        }
    }

    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return proposedContentOffset
    }

}

