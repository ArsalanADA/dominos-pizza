//
//  ProductCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 01.12.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire

class ProductCell: UICollectionViewCell {
    
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var productTitleLabel: UILabel!
    @IBOutlet var productIngredientsLabel: UILabel!
    @IBOutlet var likeButton: UIButton!

    var productCode: String?
    var isFavorite: Bool?

    override func awakeFromNib() {
        
        layer.cornerRadius = 20
        clipsToBounds = true
        
        productTitleLabel.adjustsFontSizeToFitWidth = true
//        productIngredientsLabel.adjustsFontSizeToFitWidth = true

        productImageView.layer.cornerRadius = productImageView.frame.height / 2.0
    }

    @IBAction func addToFavorite() {
        let addTofavoriteURL = baseURL + "user/favorites"
        let deleteFromfavoriteURL = baseURL + "user/delete-favorite"

        guard let user = currentUser() else {return}
        guard let code = productCode else {
            print("No Code 3334")
            return
        }

        guard let isFavorite = isFavorite else {return}

        print(user.token)

        let parameters: Parameters = ["ProductCode" : code, "osgCode" : code]
        let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]

        Alamofire.request(isFavorite ? deleteFromfavoriteURL : addTofavoriteURL, method: isFavorite ? .delete : .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: header).responseJSON { (response) in

            print(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "dodnodn")
            print(response)
        }

    }

}
