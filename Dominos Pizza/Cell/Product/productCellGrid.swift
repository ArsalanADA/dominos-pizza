//
//  productCellGrid.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 10.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class productCellGrid: UICollectionViewCell {
    
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var productTitleLabel: UILabel!
    @IBOutlet var productIngredientsLabel: UILabel!
    @IBOutlet var likeButton: UIButton!
    
    override func awakeFromNib() {
        layer.cornerRadius = 20
        clipsToBounds = true
        
        productTitleLabel.adjustsFontSizeToFitWidth = true
//        productIngredientsLabel.adjustsFontSizeToFitWidth = true

        productImageView.layer.cornerRadius = productImageView.frame.height / 2
        productImageView.clipsToBounds = true
        
    }
}
