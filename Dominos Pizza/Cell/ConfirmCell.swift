//
//  ConfirmCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 12.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Localize_Swift

class ConfirmCell: UITableViewCell {
    
    @IBOutlet var cashView: UIView!
    @IBOutlet var cardView: UIView!
    
    @IBOutlet var crewLabel: UILabel!
    
    @IBOutlet var orderDateLabel: UILabel!
    @IBOutlet var customerNameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var serviceMethodLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var additionalInfoTextView: UITextView!
    
    @IBOutlet var importanLabel: UILabel!
    
    @IBOutlet var emailCheckLabel: UILabel!
    
    @IBOutlet var agreeButton: UIButton!
    
    @IBOutlet weak var orderNOwButton: UIButtonX!

    var isAgree: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear

        fit()
        setupDetails()

        cardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cardPressed)))
        cashView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cashPressed)))

    }

    @objc func cardPressed() {
        paymetMethod = .card
        cashView.backgroundColor = .clear
        cardView.backgroundColor = blueColor
    }

    @objc func cashPressed() {
        paymetMethod = .cash
        cashView.backgroundColor = blueColor
        cardView.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fit() {
        crewLabel.adjustsFontSizeToFitWidth = true
        importanLabel.adjustsFontSizeToFitWidth = true
        emailCheckLabel.adjustsFontSizeToFitWidth = true
        
        orderDateLabel.adjustsFontSizeToFitWidth = true
        customerNameLabel.adjustsFontSizeToFitWidth = true
        numberLabel.adjustsFontSizeToFitWidth = true
        emailLabel.adjustsFontSizeToFitWidth = true
        addressLabel.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func agreePressed() {
        if !isAgree {
            agreeButton.setBackgroundImage(#imageLiteral(resourceName: "checkBoxIcon"), for: .normal)
            orderNOwButton.isEnabled = true
        } else {
            agreeButton.setBackgroundImage(nil, for: .normal)
            orderNOwButton.isEnabled = false
        }
        isAgree = !isAgree
    }

    func setupDetails() {
        if let data = UserDefaults.standard.data(forKey: userKeyString),
            let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User {

            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
            let someDateTime = formatter.string(from: date)

            orderDateLabel.text = "\(someDateTime)"
            customerNameLabel.text = user.firstname + " " + user.lastname
            numberLabel.text = user.phone
            emailLabel.text = user.email

            switch delivaryMethod {
            case .carryOut:
                serviceMethodLabel.text = "Carry Out"
            case .delivery:
                serviceMethodLabel.text = "Delivery"
            }

            addressLabel.text = user.selectedAddress()?.streetName
        } else {
            print("no user found")
        }
    }

    @IBAction func orderNowPressed() {
        let dictionary = generateOrderDictionary(with: additionalInfoTextView.text)
        let orderNowURL = URL(string: "\(baseURL)order/create")
        
        guard
            let data = UserDefaults.standard.data(forKey: userKeyString),
            let user: User = NSKeyedUnarchiver.unarchiveObject(with: data) as? User,
            orderNowURL != nil else {return}
        
        let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]
        
        Alamofire.request(orderNowURL!, method: .post, parameters: dictionary, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in

            print(response)

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)

                    print(json["data"])

                    if let trackingCode = json["data"]["trackingCode"].string {
                        UserDefaults.standard.set(trackingCode, forKey: trackingCode)
                    }

                    NotificationCenter.default.post(name: .orderSubmitted, object: nil, userInfo: ["response": json["data"].dictionaryObject ?? "no value"])
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}












