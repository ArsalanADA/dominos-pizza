//
//  OrderPizzaSebetimCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 23.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class OrderPizzaSebetimCell: UITableViewCell {
    
    @IBOutlet weak var addsTableView: UITableView!
    
    @IBOutlet var orderTitle: UILabel!
    @IBOutlet var orderQuantity: UILabel!
    @IBOutlet var orderPrice: UILabel!
    @IBOutlet var stepper: UIStepper!
    @IBOutlet weak var deleteButton: UIButton!

    var alreadyReloaded = false
    
    var orderObject: Order? {
        didSet {
            guard let order = orderObject else {return}
            guard let pizza = order.food as? AbstractPizza else {return}
            self.addsTableView.reloadData()
            self.orderTitle.text = pizza.name

            self.orderPrice.attributedText = convertToPrice(price: findSelectedProduct(in: pizza)?.price ?? 0.0 * Double(order.quantity), color: .white)
            
            self.orderQuantity.text =  "\(order.quantity)"
            self.stepper.value = Double((order.quantity))
        }
    }

    @IBAction func editPressed() {
        guard orderObject != nil else {return}
        NotificationCenter.default.post(name: .orderChangePressed, object: nil, userInfo: ["orderObject": orderObject!])
    }

    @IBAction func deletePressed() {
        if let indexOfObjectToBeDeleted = findIndex(of: orderObject) {
            orders.remove(at: indexOfObjectToBeDeleted)
            NotificationCenter.default.post(name: .orderDeleted, object: nil)
        }
    }

    @IBAction func stepperPressed(_ sender: UIStepper) {
        if let indexOfOrder = findIndex(of: orderObject) {
            orders[indexOfOrder].quantity = Int(sender.value)

            self.orderQuantity.text = "\(orders[indexOfOrder].quantity)"

            guard let pizza = orders[indexOfOrder].food as? AbstractPizza else { return }

            self.orderPrice.attributedText = convertToPrice(price: findSelectedProduct(in: pizza)?.price ?? 0.0 * Double(orders[indexOfOrder].quantity), color: .white)

            orderObject = orders[indexOfOrder]
            NotificationCenter.default.post(name: .stepperValueChanged, object: nil)
        }
    }

}

extension OrderPizzaSebetimCell: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard let pizza = orderObject?.food as? AbstractPizza else {return 0}
        let numberOfSelectedIngredients = pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients().count
        let number = numberOfSelectedIngredients + pizza.selectedToppings().count

        pizzaAddsCellHeight = CGFloat(number * 44 + 151 + 11 + 10)
        if !alreadyReloaded {
            alreadyReloaded = true
            NotificationCenter.default.post(name: .pizzaAddDeleted, object: nil)
        }
        /* 44 is height of tableCell
         */

        return number
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pizzaOrderCell") as! PizzaAddsTableCell
        guard let pizza = orderObject?.food as? AbstractPizza else {return UITableViewCell()}

        let numberOfSelectedIngredients = pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients().count

        if indexPath.row < numberOfSelectedIngredients {
            cell.nameOfIngredientLabel.text = pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients()[indexPath.row].name
            let price = pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients()[indexPath.row].price
            if price != 0 {
                cell.priceLabel.attributedText = convertToPrice(price: price, isPlus: true)
            } else {
                cell.priceLabel.text = ""
            }
        } else {
            let selectedToppings = pizza.selectedToppings()
            cell.nameOfIngredientLabel.text = selectedToppings[indexPath.row - numberOfSelectedIngredients].name
            let price = selectedToppings[indexPath.row - numberOfSelectedIngredients].price
            if price != 0 {
                cell.priceLabel.attributedText = convertToPrice(price: price, isPlus: true)
            } else {
                cell.priceLabel.text = ""
            }
        }

        cell.pizzaOrder = pizza
        cell.indexOfAdd = indexPath.row
        return cell
    }

    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }

}






