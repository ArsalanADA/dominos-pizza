//
//  PizzaAddsTableCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 23.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class PizzaAddsTableCell: UITableViewCell {

    @IBOutlet weak var nameOfIngredientLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    var pizzaOrder: AbstractPizza?
    var indexOfAdd = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameOfIngredientLabel.adjustsFontSizeToFitWidth = true
        priceLabel.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func deleteAddPressed() {
        guard let pizza = pizzaOrder else {return}
        let numberOfExtraingredients = pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients().count
        
        if indexOfAdd < numberOfExtraingredients {
            let extraIngredient = pizza.pizzas[pizza.selectedSize.rawValue].selectedExtraIngredients()[indexOfAdd]
            pizza.pizzas[pizza.selectedSize.rawValue].removeExtraIngredient(extraIngredient)
        } else {
            let topping = pizza.selectedToppings()[indexOfAdd - numberOfExtraingredients]
            pizza.remove(topping)
        }
        NotificationCenter.default.post(name: .pizzaAddDeleted, object: nil)
    }
    
    
}














