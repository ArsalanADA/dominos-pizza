//
//  OrderSebetimCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 13.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

class OrderSebetimCell: UITableViewCell {
    
    @IBOutlet var orderTitle: UILabel!
    @IBOutlet var orderQuantity: UILabel!
    @IBOutlet var orderPrice: UILabel!
    @IBOutlet var stepper: UIStepper!
    @IBOutlet weak var deleteButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustTofit()
        layout()
    }
    
    var orderObject: Order?  {
        didSet {
            self.orderTitle.text = orderObject?.food.name
            self.orderPrice.attributedText = convertToPrice(price: orderObject?.food.surchargelessPrice ?? -1 * Double(orderObject?.quantity ?? 1000), color: .white)
            
            self.orderQuantity.text = "\(orderObject?.quantity ?? -1)"
            self.stepper.value = Double((orderObject?.quantity)!)
        }
    }
    
    
    @IBAction func deletePressed() {

        // find index of object
        var indexOfObjectToBeDeleted = -1
        for i in 0 ..< orders.count {
            if orders[i].food.productCode == orderObject?.food.productCode {
                indexOfObjectToBeDeleted = i
            }
        }
        orders.remove(at: indexOfObjectToBeDeleted)
        NotificationCenter.default.post(name: .orderDeleted, object: nil)
    }
    
  
    
    func translateFalse() {
        orderTitle.translatesAutoresizingMaskIntoConstraints = false
        orderQuantity.translatesAutoresizingMaskIntoConstraints = false
        orderPrice.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func layout() {
        
    }
    
    func adjustTofit() {
        orderTitle.adjustsFontSizeToFitWidth = true
        orderQuantity.adjustsFontSizeToFitWidth = true
        orderPrice.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func stepperPressed(_ sender: UIStepper) {
        guard orderObject != nil else {return}
        orderObject?.quantity = Int(sender.value)
        
        self.orderQuantity.text = "\(orderObject?.quantity ?? -1)"

        self.orderPrice.attributedText = convertToPrice(price: orderObject?.food.surchargelessPrice ?? -1 * Double(orderObject?.quantity ?? 1000), color: .white)
        
        NotificationCenter.default.post(name: .stepperValueChanged, object: nil)
    }
}














