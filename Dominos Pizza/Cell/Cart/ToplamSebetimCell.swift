//
//  ToplamSebetimCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 21.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ToplamSebetimCell: UITableViewCell {
    
    @IBOutlet var toplamLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.toplamLabel.adjustsFontSizeToFitWidth = true

        NotificationCenter.default.addObserver(self, selector: #selector(recalculate), name: .stepperValueChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(recalculate), name: .orderDeleted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(recalculate), name: .pizzaAddDeleted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(recalculate), name: .delivaryMethodChanged, object: nil)

        toplamLabel.text = "total".localized()
        
        recalculate()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func recalculate() {
        var totalPrice = -1.0

        let dictionary: Parameters = generateOrderDictionary()
        let calculateURL = baseURL + "order/calculate-price"

        if let theJSONData = try? JSONSerialization.data(withJSONObject: dictionary, options: [.prettyPrinted]) {
            let theJSONText = String(data: theJSONData, encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }

        guard
            let data = UserDefaults.standard.data(forKey: userKeyString),
            let user: User = NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            else {return}

        let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]

        Alamofire.request(calculateURL, method: .post, parameters: dictionary, encoding: JSONEncoding.default, headers: header).responseJSON { response in

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    totalPrice = json["data"]["price"].doubleValue
                    self.toplamLabel.attributedText = convertToPrice(price: totalPrice, fontSize: 30, color: .white)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

