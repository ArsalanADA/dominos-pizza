//
//  DetailCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 24.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    
    @IBOutlet var addressTitleLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var phoneTitleLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    
    @IBOutlet weak var checkoutButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMembershipInfo), name: .membershipInfoChanged, object: nil)

        addressTitleLabel.text = "address".localized()
        phoneTitleLabel.text = "phone".localized()
        checkoutButton.setTitle("checkout".localized(), for: .normal)
        
        showMembershipInfo()
    }

    @IBAction func addressEditPressed() {
        NotificationCenter.default.post(name: .addressEditPressed, object: nil)
    }

    @IBAction func phoneEditPressed() {
        NotificationCenter.default.post(name: .phoneEditPressed, object: nil)
    }

    @IBAction func showCheckout() {
        NotificationCenter.default.post(name: .checkoutPressed, object: nil)
    }
    
    @objc func showMembershipInfo() {
        if let data = UserDefaults.standard.data(forKey: userKeyString),
            let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User {
            addressLabel.text = user.selectedAddress()?.streetName
            phoneLabel.text = user.phone
        } else {
            addressLabel.text = ""
            phoneLabel.text = ""
        }
    }

}




