//
//  OrderHistoryTableViewCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.05.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class OrderHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        idLabel.adjustsFontSizeToFitWidth = true
        storeLabel.adjustsFontSizeToFitWidth = true
        dateLabel.adjustsFontSizeToFitWidth = true
        priceLabel.adjustsFontSizeToFitWidth = true
        
    }

    /*
     "OrderID": "23787",
     "OrderText": "1 adet Medium Americano,Classic Dough,Classic (Ketchup istiyorumGreen Pepper istiyorumMozzarella Cheese istiyorumMushrooms istiyorumPizza Sauce istiyorumGrilled Chicken istiyorum)",
     "Status": "New Order",
     "Store": "Nərimanov",
     "PaymentPrice": "15.2",
     "CreatedOn": "2018-05-14 13:05:52"
 */
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
