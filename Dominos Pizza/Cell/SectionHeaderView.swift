//
//  SectionHeaderView.swift
//  Dominos Pizza
//
//  Created by Фархад on 6/21/18.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class SectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var titleCustom: String! {
        didSet {
            titleLabel.text = titleCustom
        }
    }
}
