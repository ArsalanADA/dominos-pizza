//
//  AddressCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 14.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Alamofire

class AddressCell: UITableViewCell {

    @IBOutlet var addressTitleLabel: UILabel!
    @IBOutlet weak var starButton: UIButton!

    var indexOfAddress: Int? {
        didSet {
            if myAddresses[indexOfAddress!].isFavorite {
                starButton.setBackgroundImage(#imageLiteral(resourceName: "star-filled-100"), for: .normal)
            } else {
                starButton.setBackgroundImage(#imageLiteral(resourceName: "star-100"), for: .normal)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func starPressed() {

        guard indexOfAddress != nil else {return}

        // Unfavorite all
        for i in 0 ..< myAddresses.count {
            myAddresses[i].isFavorite = false
        }

        // Davorite selected
        myAddresses[indexOfAddress!].isFavorite = true
        UserDefaults.standard.set(myAddresses[indexOfAddress!].customerAddressID, forKey: "custometAddressID")

        // Post notification to refresh all images
        NotificationCenter.default.post(name: .starPressed, object: nil)
    }

    @IBAction func deleteAddressPressed() {
        guard let user = currentUser() else {return}

        guard indexOfAddress != nil, !myAddresses.isEmpty else {return}

        let deleteURL = baseURL + "user/delete-address/\(myAddresses[indexOfAddress ?? -1].customerAddressID ?? "-1")"
        let header: HTTPHeaders = ["Authorization": "Bearer \(user.token)"]

        Alamofire.request(deleteURL, method: .delete, parameters: [:], encoding: URLEncoding.httpBody, headers: header).validate().responseJSON { (response) in

            print(response)
            NotificationCenter.default.post(name: .addressDeleted, object: nil)
        }
    }


}
