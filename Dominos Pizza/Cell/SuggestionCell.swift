//
//  SuggestionCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 24.04.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class SuggestionCell: UITableViewCell {
    
    
    @IBOutlet weak var suggestionLabel: UILabel!
    
    @IBOutlet weak var saladImageView: UIButton!
    @IBOutlet weak var saladTitleLabel: UILabel!

    @IBOutlet weak var dessertImageview: UIButton!
    @IBOutlet weak var dessertsLabel: UILabel!

    @IBOutlet weak var drinkImageView: UIButton!
    @IBOutlet weak var drinkLabel: UILabel!

    var counter = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        _ = Timer.scheduledTimer(timeInterval: Double(arc4random_uniform(1)) + 1.0, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        
        suggestionLabel.text = "suggestion".localized()
        saladTitleLabel.text = "Salad".localized()
        dessertsLabel.text = "Dessert".localized()
        drinkLabel.text = "Drink".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @objc func runTimedCode() {

        switch counter {
        case 1:
            saladImageView.shake()
        case 2:
            dessertImageview.shake()
        case 3:
            drinkImageView.shake()
            counter = 0
        default: break
        }

        counter += 1
    }


    @IBAction func presentSuggestion(_ sender: UIButton) {

        switch sender.tag {
        case 0:
            print("salad")
            model = salads
        case 1:
            print("dessert")
            model = desserts
        case 2:
            print("drink")
            model = drinks
        default:
            print("ASLDKLASKDLAKSLD")
        }

        NotificationCenter.default.post(name: .suggestionPressed, object: nil)
    }
}






