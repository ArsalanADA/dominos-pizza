//
//  PromotionCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 08.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit
import Localize_Swift

class PromotionCell: UICollectionViewCell {
    
    @IBOutlet var promotionImageView: UIImageView!
    @IBOutlet var promotionTitle: UILabel!
    
    var promotion: Promotion? {
        didSet {
            downloadImageOfPromotion(with: promotion?.CouponCode ?? "")
        }
    }
    
    override func awakeFromNib() {
        promotionTitle.adjustsFontSizeToFitWidth = true
    }
    
    func downloadImageOfPromotion(with code: String) {
        let url = URL(string: "http://www.dominospizza.az/rest/medium/Coupon/Detail/\(code)/Resize/280x280/\(Localize.currentLanguage().uppercased())/view")!

        getData(from: url) { (data, response, error) in
            if let image = UIImage(data: data!) {
                DispatchQueue.main.async {
                    self.promotionImageView.image = image
                }
            }
        }
    }
    
}
