//
//  MenuTableViewCell.swift
//  
//
//  Created by Arsalan Iravani on 08.02.2018.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
}
