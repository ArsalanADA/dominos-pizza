//
//  SizeCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 23.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

class SizeCell: UITableViewCell {


    @IBOutlet weak var smallImage: UIButton!
    @IBOutlet weak var mediumImage: UIButton!
    @IBOutlet weak var largeImage: UIButton!

    @IBOutlet var smallPriceLabel: UILabel!
    @IBOutlet var mediumPriceLabel: UILabel!
    @IBOutlet var bigPriceLabel: UILabel!
    
    @IBOutlet var smallButton: UIButtonX!
    @IBOutlet var mediumButton: UIButtonX!
    @IBOutlet var bigButton: UIButtonX!
    
    @IBOutlet var selectSizeLabel: UILabelX!

    var isSelecting: Bool? {
        didSet {
            guard foodObject != nil else {return}

            if isSelecting! {
                switch foodObject?.productSizeCode {
                case "8":
                    mediumImage.isHidden = true
                    mediumButton.isHidden = true
                    mediumPriceLabel.isHidden = true

                    largeImage.isHidden = true
                    bigButton.isHidden = true
                    bigPriceLabel.isHidden = true
                    
                    smallButton.borderColor = redColor
                    smallButton.backgroundColor = redColor
                    
                    guard let pizza = foodObject as? AbstractPizza else {return}
                    pizza.selectedSize = returnSize("8")
                case "10":
                    smallImage.isHidden = true
                    smallButton.isHidden = true
                    smallPriceLabel.isHidden = true

                    largeImage.isHidden = true
                    bigButton.isHidden = true
                    bigPriceLabel.isHidden = true
                    
                    mediumButton.borderColor = redColor
                    mediumButton.backgroundColor = redColor
                    
                    guard let pizza = foodObject as? AbstractPizza else {return}
                    pizza.selectedSize = returnSize("10")
                case "12":
                    smallImage.isHidden = true
                    smallButton.isHidden = true
                    smallPriceLabel.isHidden = true

                    largeImage.isHidden = true
                    bigButton.isHidden = true
                    bigPriceLabel.isHidden = true
                    
                    bigButton.borderColor = redColor
                    bigButton.backgroundColor = redColor
                    
                    guard let pizza = foodObject as? AbstractPizza else {return}
                    pizza.selectedSize = returnSize("12")
                default: break
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        smallButton.setTitle("small".localized(), for: .normal)
        mediumButton.setTitle("medium".localized(), for: .normal)
        bigButton.setTitle("big".localized(), for: .normal)
        
        self.selectSizeLabel.text = "selectSize".localized()

        assignPrice()
    }

    @objc func assignPrice() {

        guard let pizza = foodObject as? AbstractPizza else { return }
        for product in pizza.products {
            if product.doughCode == pizza.pizzas[pizza.selectedSize.rawValue].selectedDough()?.code {
                if product.sideCode == pizza.pizzas[pizza.selectedSize.rawValue].selectedRim()?.sideCode {
                    switch product.size {
                    case .small:
                        smallPriceLabel.attributedText = convertToPrice(price: product.price, color: .white)
                    case .medium:
                        mediumPriceLabel.attributedText = convertToPrice(price: product.price, color: .white)
                    case .big:
                        bigPriceLabel.attributedText = convertToPrice(price: product.price, color: .white)
                    }
                }
            }
        }

        switch pizza.selectedSize {
        case .small:
            smallButton.borderColor = redColor
            smallButton.backgroundColor = redColor
            
        case .medium:
            mediumButton.borderColor = redColor
            mediumButton.backgroundColor = redColor
            
        case .big:
            bigButton.borderColor = redColor
            bigButton.backgroundColor = redColor
        }
    }


    @IBAction func sizeButtonPressed(_ sender: UIView) {
        guard let pizza = foodObject as? AbstractPizza else { return }
        
        switch sender.tag {
        case 0:
            resetColors()
            smallButton.borderColor = redColor
            smallButton.backgroundColor = redColor
            pizza.selectedSize = .small
            
        case 1:
            resetColors()
            mediumButton.borderColor = redColor
            mediumButton.backgroundColor = redColor
            pizza.selectedSize = .medium
            
        case 2:
            resetColors()
            bigButton.borderColor = redColor
            bigButton.backgroundColor = redColor
            pizza.selectedSize = .big
            
        default:
            break
        }

        NotificationCenter.default.post(name: .sizeChanged, object: nil)
    }
    
    func resetColors() {
        smallButton.backgroundColor = .clear
        mediumButton.backgroundColor = .clear
        bigButton.backgroundColor = .clear
        
        smallButton.borderColor = blueColor
        mediumButton.borderColor = blueColor
        bigButton.borderColor = blueColor
    }
    
}
