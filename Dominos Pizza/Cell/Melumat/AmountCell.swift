//
//  AmountCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 01.12.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

var orderQuantityFromStepper: Double = -1

class AmountCell: UITableViewCell {
    
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var stepper: UIStepper!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    let fontSize: CGFloat = 30

    var isChanging: Bool? = false
    var isSelecting: Bool? = false

    var isPizzaSelected: Bool? {
        didSet {
            if isPizzaSelected ?? false {
                if foodObject is AbstractPizza {
                    showPrice()
                } else {
                    priceLabel.text = ""
                    activityIndicator.startAnimating()
                }
            } else {
                priceLabel.attributedText = convertToPrice(price: foodObject?.surchargelessPrice ?? -1.0, fontSize: fontSize)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        if isChanging ?? false {
            stepper.isHidden = true
        }

        stepper.value = orderQuantityFromStepper
        quantityLabel.text = "\(Int(orderQuantityFromStepper))"

        priceLabel.adjustsFontSizeToFitWidth = true
        quantityLabel.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .detailsDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .reload22, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .stepperChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .rimChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .sizeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .ingredientChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .doughTypeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPrice), name: .extraIngredientChanged, object: nil)
    }
    
    @objc func showPrice() {
        if foodObject is AbstractPizza {
            guard var price = calculatePrice(of: foodObject) else {return}
            price *= orderQuantityFromStepper
            priceLabel.attributedText = convertToPrice(price: price, fontSize: fontSize)
        } else {
            priceLabel.attributedText = convertToPrice(price: foodObject?.surchargelessPrice ?? -1.0, fontSize: fontSize)
        }
        activityIndicator.stopAnimating()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func changeValueOfStepper(_ sender: UIStepper) {
        orderQuantityFromStepper = sender.value
        quantityLabel.text = "\(Int(orderQuantityFromStepper))"
        NotificationCenter.default.post(name: .stepperChanged, object: nil)
    }
}











