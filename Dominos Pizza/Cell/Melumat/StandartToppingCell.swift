//
//  StandartToppingCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 16.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class StandartToppingCell: UICollectionViewCell {
    
    @IBOutlet var button: UIButtonX!
    
    var index: Int? {
        didSet {
            guard let pizza = foodObject as? AbstractPizza else {return}
            if pizza.ingredients[index!].isSelected {
                button.borderColor = redColor
                button.backgroundColor = redColor
            } else {
                button.borderColor = blueColor
                button.backgroundColor = .clear
            }
        }
    }
    
    @IBAction func buttonPressed() {
        guard let index = index else { return }
        guard let pizza = foodObject as? AbstractPizza else {return}
        
        if !pizza.ingredients[index].isSelected {
            pizza.ingredients[index].isSelected = true
        } else {
            pizza.ingredients[index].isSelected = false
        }
        
        NotificationCenter.default.post(name: .ingredientChanged, object: nil)
    }
}
