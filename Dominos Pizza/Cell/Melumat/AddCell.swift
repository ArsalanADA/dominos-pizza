//
//  AddCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 13.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class AddCell: UITableViewCell {

    @IBOutlet var addNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addNameLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
