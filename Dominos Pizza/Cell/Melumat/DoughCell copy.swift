//
//  DoughCell.swift
//  
//
//  Created by Arsalan Iravani on 12.01.2018.
//

import UIKit

class DoughCell: UITableViewCell {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var selectDoughLabel: UILabelX!

    var doughLayout: RimCellFlowLayout!

    override func awakeFromNib() {
        super.awakeFromNib()

        doughLayout = RimCellFlowLayout(numberOfColumns: 3)

        selectDoughLabel.text = "selectDough".localized()
        self.collectionView.setCollectionViewLayout(doughLayout, animated: false)

        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .detailsDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .sizeChanged, object: nil)
    }
    
    @objc func reload() {
        self.collectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


extension DoughCell: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return doughLayout.itemSize
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let pizza = foodObject as? AbstractPizza else { return 0 }

        let doughsCount: Int = pizza.pizzas[pizza.selectedSize.rawValue].dough.count
        let lines: Int = (doughsCount + doughLayout.numberOfCellsInRow - 1) / doughLayout.numberOfCellsInRow
        let spacing = CGFloat(lines - 1) * doughLayout.minimumLineSpacing + 72
        doughCC = CGFloat(lines) * doughLayout.itemSize.height + doughLayout.sectionInset.top + doughLayout.sectionInset.bottom + spacing
        
        return doughsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let pizza = foodObject as? AbstractPizza else { return UICollectionViewCell() }
        
        let cell: MelumatSelectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "melumatSelectionCell", for: indexPath) as! MelumatSelectionCell
        
        cell.imageView.image = pizza.pizzas[pizza.selectedSize.rawValue].dough[indexPath.row].image
        cell.button.setTitle(pizza.pizzas[pizza.selectedSize.rawValue].dough[indexPath.row].name, for: .normal)
        if pizza.pizzas[pizza.selectedSize.rawValue].dough[indexPath.row].price != 0 {
            cell.priceLabel.attributedText = convertToPrice(price: pizza.pizzas[pizza.selectedSize.rawValue].dough[indexPath.row].price, fontSize: 15, color: .white, isPlus: true)
        } else {
            cell.priceLabel.text = ""
        }

        if pizza.pizzas[pizza.selectedSize.rawValue].dough[indexPath.row].isSelected {
            cell.button.borderColor = redColor
            cell.button.backgroundColor = redColor
        } else {
            cell.button.borderColor = blueColor
            cell.button.backgroundColor = .clear
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        guard let pizza = foodObject as? AbstractPizza else { return }

        resetAll()
        pizza.pizzas[pizza.selectedSize.rawValue].dough[indexPath.row].isSelected = !pizza.pizzas[pizza.selectedSize.rawValue].dough[indexPath.row].isSelected

        reload()
        NotificationCenter.default.post(name: .doughTypeChanged, object: nil)
    }

    func resetAll() {
        guard let pizza = foodObject as? AbstractPizza else {return}

        for i in 0 ..< pizza.pizzas[pizza.selectedSize.rawValue].dough.count {
            pizza.pizzas[pizza.selectedSize.rawValue].dough[i].isSelected = false
        }
    }
}









