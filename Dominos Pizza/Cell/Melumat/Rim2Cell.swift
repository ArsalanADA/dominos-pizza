//
//  Rim2Cell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 16.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class Rim2Cell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var button: UIButtonX!
    @IBOutlet var priceLabel: UILabel!

    override func awakeFromNib() {
        button.titleLabel?.minimumScaleFactor = 0.1
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        priceLabel.adjustsFontSizeToFitWidth = true
    }
    
}
