//
//  RimCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 12.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class RimCell: UITableViewCell {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var selectRimLabel: UILabelX!

    var rimLayout: RimCellFlowLayout!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        collectionView.delegate = self

        rimLayout = RimCellFlowLayout(numberOfColumns: 3)

        self.selectRimLabel.text = "selectRim".localized()
        self.collectionView.setCollectionViewLayout(rimLayout, animated: false)

        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .detailsDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(doughChanged), name: .doughTypeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .sizeChanged, object: nil)

    }
    
    @objc func reload() {
        self.collectionView.reloadData()
    }

    @objc func doughChanged() {
        guard let pizza = foodObject as? AbstractPizza else {return}
        pizza.setToClassicRim()
        reload()
    }

}

extension RimCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return rimLayout.itemSize
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let pizza = foodObject as? AbstractPizza else { return 0 }

        let sidesCount = pizza.pizzas[pizza.selectedSize.rawValue].rim.count

        let lines = (sidesCount + rimLayout.numberOfCellsInRow - 1) / rimLayout.numberOfCellsInRow
        let spacing = CGFloat(lines - 1) * rimLayout.minimumLineSpacing + 70
        rimCC = CGFloat(lines) * rimLayout.itemSize.height + rimLayout.sectionInset.top + rimLayout.sectionInset.bottom + spacing
        return sidesCount
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let pizza = foodObject as? AbstractPizza else {return}

        clearAll()
        pizza.pizzas[pizza.selectedSize.rawValue].rim[indexPath.row].isSelected = !pizza.pizzas[pizza.selectedSize.rawValue].rim[indexPath.row].isSelected

        reload()
        NotificationCenter.default.post(name: .rimChanged, object: nil)
    }

    func clearAll() {
        guard let pizza = foodObject as? AbstractPizza else {return}

        for i in 0 ..< pizza.pizzas[pizza.selectedSize.rawValue].rim.count {
            pizza.pizzas[pizza.selectedSize.rawValue].rim[i].isSelected = false
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let rim2Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "rim2", for: indexPath) as! Rim2Cell
        
        guard let pizza = foodObject as? AbstractPizza else {return UICollectionViewCell()}
        let rims = pizza.pizzas[pizza.selectedSize.rawValue].rim
        
        rim2Cell.imageView.image = rims[indexPath.row].image
        rim2Cell.button.setTitle(rims[indexPath.row].name, for: .normal)
        if rims[indexPath.row].price != 0 {
            rim2Cell.priceLabel.attributedText = convertToPrice(price: rims[indexPath.row].price, fontSize: 15, color: .white, isPlus: true)
        } else {
            rim2Cell.priceLabel.text = ""
        }

        if pizza.pizzas[pizza.selectedSize.rawValue].rim[indexPath.row].isSelected {
            rim2Cell.button.borderColor = redColor
            rim2Cell.button.backgroundColor = redColor
        } else {
            rim2Cell.button.borderColor = blueColor
            rim2Cell.button.backgroundColor = .clear
        }

        return rim2Cell
    }
    
    
}




