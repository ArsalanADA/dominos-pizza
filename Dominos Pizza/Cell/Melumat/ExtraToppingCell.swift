//
//  ExtraToppingCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 12.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class ExtraToppingCell: UITableViewCell {
    
    @IBOutlet var selectExtraLabel: UILabelX!
    @IBOutlet var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        selectExtraLabel.text = "selectExtra".localized()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .detailsDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .extraIngredientChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .sizeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .doughTypeChanged, object: nil)
    }
    
    @objc func reload() {
        collectionView.reloadData()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}


extension ExtraToppingCell: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                guard let pizza = foodObject as? AbstractPizza else {return 0}

                let count: CGFloat = CGFloat(pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients.count)
                let numberOfColumns: Int = Int(UIScreen.main.bounds.width / 169)
                let numberOfRows: CGFloat = CGFloat(Int((count + 1) / CGFloat(numberOfColumns)))
                exToppings = 78 * numberOfRows + 150

                return pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let pizza = foodObject as? AbstractPizza else {return UICollectionViewCell()}
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "extraTopping", for: indexPath) as! ExtraTopping2Cell
        cell.titleLabel.text = pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients[indexPath.row].name
        if pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients[indexPath.row].price != 0 {
            cell.priceLabel.attributedText = convertToPrice(price: pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients[indexPath.row].price, fontSize: 15, color: .white, isPlus: true)
        } else {
            cell.priceLabel.text = ""
        }
        
        cell.index = indexPath.row
        
        return cell
    }
    
    
    
    
}
