//
//  DescriptionCell.swift
//  
//
//  Created by Arsalan Iravani on 12.01.2018.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

         NotificationCenter.default.addObserver(self, selector: #selector(t), name: .detailsDownloaded, object: nil)

        nameLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.adjustsFontSizeToFitWidth = true

        guard let pizza = foodObject as? AbstractPizza else {return}
        nameLabel.text = pizza.name
        
        descriptionLabel.text = flatArrayOfIngredients(pizza.ingredients)
    }

    @objc func t() {
        guard let pizza = foodObject as? AbstractPizza else {return}
        descriptionLabel.text = flatArrayOfIngredients(pizza.ingredients)
    }
}
