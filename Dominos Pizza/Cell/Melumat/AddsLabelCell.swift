//
//  AddsLabelCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 13.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class AddsLabelCell: UITableViewCell {

    @IBOutlet var addLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
            addLabel.text = "adds".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
