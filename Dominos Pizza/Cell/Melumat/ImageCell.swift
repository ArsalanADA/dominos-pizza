//
//  ImageCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 23.10.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
    
    @IBOutlet var productImageView: UIImageView!
    
    var image2: UIImage? {
        didSet {
            productImageView.image = image2
        }
    }
}
