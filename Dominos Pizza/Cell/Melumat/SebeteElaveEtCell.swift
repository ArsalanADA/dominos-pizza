//
//  SebeteElaveEtCell.swift
//  Pods-Dominos Pizza
//
//  Created by Arsalan Iravani on 23.10.2017.
//

import UIKit

class SebeteElaveEtCell: UITableViewCell {
    
    @IBOutlet var sebeteElaveEtButton: UIButton!
    
    let fontSize: CGFloat = 20
    var isChanging: Bool? {
        didSet {
            if isChanging! {
                sebeteElaveEtButton.isHidden = true
            }
        }
    }
    var isProductSelecting: Bool?

    override func awakeFromNib() {
        super.awakeFromNib()

        sebeteElaveEtButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(showTotalPrice), name: .rimChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTotalPrice), name: .sizeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTotalPrice), name: .stepperChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTotalPrice), name: .ingredientChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTotalPrice), name: .doughTypeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTotalPrice), name: .extraIngredientChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showTotalPrice), name: .pizzaAddDeleted, object: nil)
        sebeteElaveEtButton.setTitle("addToOrders".localized(), for: .normal)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func sebeteElaveEt() {
        if !isProductSelecting! {
        NotificationCenter.default.post(name: .newOrderAdded, object: nil, userInfo: ["cgpoint": self.sebeteElaveEtButton.superview?.convert(self.sebeteElaveEtButton.frame.origin, to: nil) ?? ["cgpoint": CGPoint(x: 0, y: 0)]])
        } else {
            
            let order =  Order(food: foodObject!, quantity: Int(orderQuantityFromStepper))
            promotionOrder[indexOfSelectedProduct] = order
            
            NotificationCenter.default.post(name: .newProductAdded, object: nil)
        }
    }
    
    @objc func showTotalPrice() {
    
        let combination = NSMutableAttributedString()
        
        if let f = UIFont(name: "JIS AZN", size: fontSize), let ovsankaFont = UIFont(name: "Ovsyanka", size: fontSize) {
            
            let yourAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: ovsankaFont]
            
            let yourOtherAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: f]
            
            guard foodObject != nil else {return}
            guard var price = calculatePrice(of: foodObject) else {return}
            price *= orderQuantityFromStepper
            let partOne = NSMutableAttributedString(string: "addToOrders".localized() + "       \(price)", attributes: yourAttributes)
            let partTwo = NSMutableAttributedString(string: "m", attributes: yourOtherAttributes)
            
            combination.append(partOne)
            combination.append(partTwo)
            
            sebeteElaveEtButton.setAttributedTitle(combination, for: .normal)
        }
    }
    
}
