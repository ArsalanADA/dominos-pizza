//
//  MelumatSelectionCell.swift
//  
//
//  Created by Arsalan Iravani on 12.01.2018.
//

import UIKit

class MelumatSelectionCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var button: UIButtonX!
    @IBOutlet var priceLabel: UILabel!

    override func awakeFromNib() {
        button.titleLabel?.minimumScaleFactor = 0.1
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        priceLabel.adjustsFontSizeToFitWidth = true
    }
    
}
