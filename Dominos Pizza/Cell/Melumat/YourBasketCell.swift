//
//  YourBasketCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 13.02.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class YourBasketCell: UITableViewCell {
    
    @IBOutlet var yourLabel: UILabelX!
    
    
    override func awakeFromNib() {
        yourLabel.text = "yourBasket".localized()
    }
}
