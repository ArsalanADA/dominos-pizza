//
//  StandartToppingsCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 12.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class StandartToppingsCell: UITableViewCell {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var donlikeLabel: UILabel!
    @IBOutlet var selectToppingsLabel: UILabelX!
    
    let numberOfCellInARow: CGFloat = 2
    let height: CGFloat = 45
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectToppingsLabel.text = "selectToppings".localized()
        
        donlikeLabel.text = "dontlike".localized()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .detailsDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: .ingredientChanged, object: nil)

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0

        let widthOfCell = (UIScreen.main.bounds.width - layout.sectionInset.left - layout.sectionInset.right -
            ((numberOfCellInARow - 1) * layout.minimumInteritemSpacing)) / numberOfCellInARow

        let cellSize = CGSize(width: widthOfCell, height: height)
        layout.itemSize = cellSize

        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    @objc func reload() {
        collectionView.reloadData()
    }
}

extension StandartToppingsCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let pizza = foodObject as? AbstractPizza else { return 0 }
        let count: CGFloat = CGFloat(pizza.ingredients.count)
        let numberOfRows: CGFloat = CGFloat(Int((count + 1) / 2))
        let r: CGFloat = (height + 10) * numberOfRows + 140
        stToppings = r
        return pizza.ingredients.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let pizza = foodObject as? AbstractPizza else { return UICollectionViewCell() }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "standartTopping", for: indexPath) as! StandartToppingCell
        cell.button.setTitle( pizza.ingredients[indexPath.row].name , for: .normal)
        cell.index = indexPath.row
        return cell
    }
}




