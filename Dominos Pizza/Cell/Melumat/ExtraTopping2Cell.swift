//
//  ExtraTopping2Cell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 21.01.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class ExtraTopping2Cell: UICollectionViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var button: UIButtonX!
    
    var index: Int? {
        didSet {
            guard let pizza = foodObject as? AbstractPizza else { return }
            if pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients[index!].isSelected {
                button.setBackgroundImage(#imageLiteral(resourceName: "checkBoxIcon"), for: .normal)
            } else {
                button.setBackgroundImage(UIImage(), for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        titleLabel.adjustsFontSizeToFitWidth = true
        priceLabel.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func buttonPressed() {
        guard let index = index else { return }
        
        self.isSelected = !self.isSelected
        guard let pizza = foodObject as? AbstractPizza else { return }
        
        if !pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients[index].isSelected {
            button.setBackgroundImage(#imageLiteral(resourceName: "checkBoxIcon"), for: .normal)
            pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients[index].isSelected = true
        } else {
            button.setBackgroundImage(UIImage(), for: .normal)
            pizza.pizzas[pizza.selectedSize.rawValue].extraIngredients[index].isSelected = false
        }
        
        NotificationCenter.default.post(name: .extraIngredientChanged, object: nil)
    }
}

