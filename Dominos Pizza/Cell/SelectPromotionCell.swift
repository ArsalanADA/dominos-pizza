//
//  SelectPromotionCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 24.04.2018.
//  Copyright © 2018 Arsalan Iravani. All rights reserved.
//

import UIKit

class SelectPromotionCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2.0
        self.layer.borderWidth = 5
        self.layer.borderColor = blueColor.cgColor
    }
    
}
