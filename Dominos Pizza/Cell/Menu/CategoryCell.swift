//
//  CategoryCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 29.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var PDFImageview: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
}
