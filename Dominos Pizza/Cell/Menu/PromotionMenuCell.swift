//
//  PromotionMenuCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 29.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

class PromotionMenuCell: UITableViewCell {

    @IBOutlet var backgroundImaheView: UIImageView!
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var productLabel: UILabel!
    var isPromotions: Bool? {
        didSet {
            _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productLabel.adjustsFontSizeToFitWidth = true
    }


    @objc func runTimedCode() {
        productImageView.shake()
        productLabel.shake()
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
