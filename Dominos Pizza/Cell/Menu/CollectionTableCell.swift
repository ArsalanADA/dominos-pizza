//
//  CategoriesTableCell.swift
//  Dominos Pizza
//
//  Created by Arsalan Iravani on 29.11.2017.
//  Copyright © 2017 Arsalan Iravani. All rights reserved.
//

import UIKit

var globalSizeForCollectionView: CGFloat = 100
var selectedCellIndex = -1

class CollectionTableCell: UITableViewCell, Sizeable, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var collectionView2: CollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView2.myCustom = self
        
       NotificationCenter.default.addObserver(self, selector: #selector(languageChanged), name: .languageChanged, object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(languageChanged), name: .userChanged, object: nil)
    }
    
    @objc func languageChanged() {
        self.collectionView2.reloadData()
    }
    
    func sizeOfCell(size: CGSize) {
        globalSizeForCollectionView = CGFloat((6 + 1) / 2) * size.height
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        NotificationCenter.default.post(name: NSNotification.Name("test"), object: nil)
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath as IndexPath) as! CategoryCell
        
        cell.backgroundImageView.image = categories[indexPath.row].mainImage
        cell.PDFImageview.image = categories[indexPath.row].PDFImage
        cell.titleLabel.text = categories[indexPath.row].title.localized()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCellIndex = indexPath.row
        indexSelected = 10
        switch categories[selectedCellIndex].categoryCode {
        case "Wings":
            model = chickens
            selectedCategory = .chicken
        case "Bread":
            model = breads
            selectedCategory = .bread
        case "Salad":
            model = salads
            selectedCategory = .salad
        case "Drink":
            model = drinks
            selectedCategory = .drink
        case "Desert":
            model = desserts
            selectedCategory = .dessert
        case "Souce":
            model = sauces
            selectedCategory = .sauce
        default:
            break
        }
        
        NotificationCenter.default.post(name: NSNotification.Name("showProducts"), object: nil)
    }
    
}
